# Homework 5

_Status:_ Final (although there might be some typos).

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=2803960

**Due date:** Friday, May 8.

## Regular exercises

### Permutations & Combinations

> Section 6.2 in course textbook.

1.  In the following exercises, find the number of (unordered) five-card poker
    hands, selected from an ordinary 52-card deck, having the properties
    indicated.

    i.  Of the form A2345 of the same suit.
    i.  Consecutive and of the same suit (assume that the ace is the lowest
        denomination).
    i.  Consecutive (assume that the ace is the lowest denomination).
    i.  Containing two of one denomination and three of another denomination.
        This is know as a _full house_.

1.  In the following exercises, find the number of (unordered) thirteen-card
    _bridge_ hands, selected from an ordinary 52-card deck, having the
    properties indicated.

    i.  How many bridge hands are all of the same suit?
    i.  How many bridge hands contain exactly two suits?
    i.  How many bridge hands contain all four aces?
    i.  How many bridge hands contain four cards of three suits and one card of
        the fourth suit?

1.  Assume a fair coin is flipped 10 times. Answer the following questions.

    i.  An _outcome_ is a list of 10 _heads_ (H) and _tails_ (T) that gives the
        result of each of 10 tosses. How many outcomes are possible? 
    i.  How many outcomes have exactly three heads?
    i.  How many outcomes have at most three heads?

1.  For the following exercises, refer to a shipment of 50 microprocessors of
    which four are defective.

    i.  In how many ways can we select a set of four microprocessors?
    i.  In how many ways can we select a set of four nondefective
        microprocessors?
    i.  In how many ways can we select a set of four microprocessors containing
        at least one defective microprocessor?

1.  Let $s_{n,k}$ denote the number of ways to seat $n$ persons at $k$ round
    tables, with at least one person at each table.[^one] The ordering of the
    tables is not taken into account. The seating arrangement at a table is
    taken into account except for rotations. For example, the following pairs
    are not distinct

    | ![pair1](./pair1.png) |
    |:---------------------:|
    | ![pair2](./pair2.png) |
    | _Fig 1:_ Examples of equal seating arrangements. |

    whereas the following pairs are distinct

    | ![pair3](./pair3.png) |
    |:---------------------:|
    | ![pair4](./pair4.png) |
    | _Fig 1:_ Examples of non-equal seating arrangements. |

    Answer the following questions.

    i.  Show that $s_{n,k} = 0$ if $k > n$.
    i.  Show that $s_{n,n} = 1$ for all $n \ge 1$.
    i.  Show that $s_{n,1} = (n − 1)!$ for all $n \ge 1$.
    i.  Show that $s_{n,n−1} = \binom{n}{2}$ for all $n \ge 2$.

1.  How many strings can be formed by ordering the letters SALESPERSONS if the
    four S’s must be consecutive?

1.  For the following exercises, refer to a bag containing 20 balls: six red,
    six green, and eight purple.
    
    i.  In how many ways can we select five balls if the balls are considered
        distinct?
    i.  In how many ways can we select five balls if balls of the same color are
        considered identical?
    i.  In how many ways can we draw two red, three green, and two purple balls
        if the balls are considered distinct?

---


### The Pigeonhole Principle

> Section 6.8 in course textbook.

8.  Answer the following questions.

    i.  Prove that if five cards are chosen from an ordinary 52-card deck, at
        least two cards are of the same suit.
    i.  Prove that among a group of six students, at least two received the same
        grade on the final exam (the grades assigned were chosen from A, B, C,
        D, F.).
    i.  Suppose that each person in a group of 32 people receives a check in
        January. Prove that at least two people receive checks on the same day.

8.  Suppose that six distinct integers are selected from the set $\{1, 2, \dots,
    10\}$. Prove that at least two of the six have a sum equal to 11.

    > _Hint:_ Consider the partition $\{1, 10\}$, $\{2, 9\}$, $\{3, 8\}$, $\{4,
    > 7\}$, $\{5, 6\}$.

8.  Professor _Salazar_ is paid every other week on Friday. Show that in some
    month he is paid three times.

---

##  Miscellaneous exercises

*   Let $S_{n,k}$ denote the number of ways to partition an $n$-element set into
    exactly $k$ nonempty subsets. The order of the subsets is not taken into
    account.[^two]

    i.  Show that $S_{n,k} = 0$ if $k > n$.
    i.  Show that $S_{n,n} = 1$ for all $n \ge 1$.
    i.  Show that $S_{n,1} = 1$ for all $n \ge 1$.
    i.  Show that $S_{n,2} = 2^{n−1} − 1$ for all $n \ge 2$.
    i.  Show that $S_{n,n−1} = \binom{n}{2}$ for all $n \ge  2$.

*   How many distinct strings can be formed by ordering the letters SCHOOL using
    some or all of the letters?

    > Are you sure you are not missing one string?


[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[^one]: The numbers $s_{n,k}$ are called _Stirling numbers of the first kind_.
[^two]: The numbers $S_{n,k}$ are called _Stirling numbers of the second kind_.

[pdf]: readme.pdf
[MAIN]: ../..

