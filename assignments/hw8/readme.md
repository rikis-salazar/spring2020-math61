# Homework 8

_Status:_ In progress. **Although more problems will be added over the week,
quiz 8 will be released on Saturday June 6. It will be based on problems
1--12.**

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=2803960

**Due date:** Friday, June 5.

## Regular exercises

### Hamiltonian Cycles

> Section 8.3 in course textbook.

1.  In the following exercises, find a Hamiltonian cycle in the corresponding
    graph.

    | ![graph01][g01] | ![graph02][g02] |
    |:---------------:|:---------------:|
    | Graph 1         |  Graph 2        |

    [g01]: ./g01.png
    [g02]: ./g02.png

    1.  Graph 1 (above)
    1.  Graph 2 (above)

1.  Explain why none of the graphs below has a Hamiltonian cycle.

    | ![graph03][g03] | ![graph04][g04] | ![graph05][g05] |
    |:---------------:|:---------------:|:---------------:|
    | Graph 3         | Graph 4         | Graph 5         |

    [g03]: ./g03.png
    [g04]: ./g04.png
    [g05]: ./g05.png

    1.  Graph 3 (above)
    1.  Graph 4 (above)
    1.  Graph 5 (above)

1.  For each of the graphs below, determine whether or not a Hamiltonian cycle
    exists. If there is one, exhibit it; otherwise, give an argument that shows
    there is none.

    | ![graph06][g06] | ![graph07][g07] | ![graph08][g08] |
    |:---------------:|:---------------:|:---------------:|
    | Graph 6         | Graph 7         | Graph 8         |

    [g06]: ./g06.png
    [g07]: ./g07.png
    [g08]: ./g08.png

    1.  Graph 6 (above)
    1.  Graph 7 (above)
    1.  Graph 8 (above)

1.  Give an example of a graph that has

    i.  an Euler cycle but contains no Hamiltonian cycle.
    i.  an Euler cycle that is also a Hamiltonian cycle.

1.  A _Hamiltonian path_ in a graph $G$ is a simple path that contains every
    vertex in $G$ exactly once.

    i.  If a graph has a Hamiltonian cycle, must it have a Hamiltonian path?
        Explain.
    i.  If a graph has a Hamiltonian path, must it have a Hamiltonian cycle?
        Explain.

---

### A shortest path algorithm

> Section 8.4 in course textbook.

6.  For the pairs of vertices below, use the following graph to find the length
    of a shortest path, as well as a shortest path between each pair of
    vertices. 

    | ![graph09][g09] |
    |:---------------:|
    | Graph 9         |

    [g09]: ./g09.png

    i. Vertices $a$ and $f$.
    i. Vertices $b$ and $j$.
    i. Vertices $h$ and $d$.

6.  Modify _Dijkstra's_ algorithm so that it finds the lengths of the shortest
    paths from a given vertex to every other vertex in a connected, weighted
    graph $G$.

6.  _Dijkstra's_ algorithm is to be applied to connected weighted graphs.
    Describe the values of the labels $L(v)$ for all vertices of $G$, if the
    graph is not connected. Assume $G$ has at least two components, and that
    both components have at least one vertex each.

---

### Representation of graphs

> Section 8.5 in course textbook.
    
9.  Write the adjacency matrix for each graph.

    | ![graph10][g10] | ![graph11][g11] | ![graph12][g12] |
    |:---------------:|:---------------:|:---------------:|
    | Graph 10        | Graph 11        | Graph 12        |

    [g10]: ./g10.png
    [g11]: ./g11.png
    [g12]: ./g12.png

    i.  Graph 10 (above)
    i.  Graph 11 (above)
    i.  Graph 12 (above)
    i.  The complete bipartite graph $K_{2,3}$.
    i.  The complete graph on five vertices $K_{5}$.

9.  Write the incidence matrix for each graph.

    i.  The complete bipartite graph $K_{2,3}$.
    i.  The complete graph on five vertices $K_{5}$.
    i.  Graph 10 (above)
    i.  Graph 11 (above)
    i.  Graph 12 (above)

9.  Draw the graph represented by each adjacency matrix.
    $$\mathbb{A} =
        \begin{pmatrix}
          2 & 0 & 0 & 1 & 0 \\
          0 & 0 & 1 & 0 & 1 \\
          0 & 1 & 2 & 1 & 1 \\
          1 & 0 & 1 & 0 & 0 \\
          0 & 1 & 1 & 0 & 0
        \end{pmatrix}, \qquad
      \mathbb{B} =
        \begin{pmatrix}
          4 & 1 & 1 & 1 & 0 & 2 \\
          1 & 0 & 1 & 1 & 1 & 0 \\
          1 & 1 & 0 & 1 & 1 & 3 \\
          1 & 1 & 1 & 0 & 1 & 1 \\
          0 & 1 & 1 & 1 & 0 & 1 \\
          2 & 0 & 3 & 1 & 1 & 0
      \end{pmatrix}.$$

    i.  Order: $a,b,c,d,e$. Matrix $\mathbb{A}$ above.
    i.  Order: $a,b,c,d,e,f$. Matrix $\mathbb{B}$ above.

9.  Suppose that a graph $G$ has an adjacency matrix of the form
    $$\mathbb{A} =
      \begin{pmatrix}
        \mathbb{A}_{1,1} & 0 \\
        0 & \mathbb{A}_{2,2}
      \end{pmatrix}$$
    where $\mathbb{A}_{1,1}$ and $\mathbb{A}_{2,2}$ are non-zero $n_{1}\times
    n_{1}$ and $n_{2}\times n_{2}$ square matrices, respectively. What must the
    graph of $G$ look like?

---

The rest of this assignment is TBA.

<!--

    | ![graph13][g13] | ![graph14][g14] | ![graph15][g15] |
    |:---------------:|:---------------:|:---------------:|
    | Graph 13        | Graph 14        | Graph 15        |

    [g13]: ./g13.png
    [g14]: ./g14.png
    [g15]: ./g15.png


---


### Paths and Cycles

> Section 8.2 in course textbook.

11. In the following exercises, tell whether the given path in the graph 14 is

    *   A simple path
    *   A cycle
    *   A simple cycle

    | ![graph14][g14] |
    |:---------------:|
    | Graph 14        |

    [g14]: ./g14.png

    i.  $(a, d, c, d, e)$
    i.  $(e, d, c, b)$
    i.  $(b, c, d, e, b, b)$
    i.  $(b, c, d, a, b, e, d, c, b)$
    i.  $(d, c, d)$

10. In the following exercises, draw a graph having the given properties or
    explain why no such graph exists.

    i.  Six vertices each of degree 3.
    i.  Four vertices each of degree 1.
    i.  Five vertices each of degree 3.
    i.  Six vertices; four edges.
    i.  Four edges; four vertices having degrees 1, 2, 3, 4.
    i.  Simple graph; six vertices having degrees 1, 2, 3, 4, 5, 5.

10. For graph 15 (below), find all

    * the simple cycles; and
    * the simple paths from $a$ to $e$.

    | ![graph15][g15] |
    |:---------------:|
    | Graph 15        |

    [g15]: ./g15.png

10. Find the degree of each vertex for the following graphs.

    | ![graph16][g16] | ![graph17][g17] |
    |:---------------:|:---------------:|
    | Graph 16        | Graph 17        |

    [g16]: ./g16.png
    [g17]: ./g17.png

10. In the following exercises, find all subgraphs having at least one vertex of
    the specified graph.

    | ![graph18][g18] | ![graph19][g19] | ![graph20][g20] |
    |:---------------:|:---------------:|:---------------:|
    | Graph 18        | Graph 19        | Graph 20        |

    [g18]: ./g18.png
    [g19]: ./g19.png
    [g20]: ./g20.png

    i.  Graph 18 (above)
    i.  Graph 19 (above)
    i.  Graph 20 (above)

10. In the following exercises, decide whether the specified graph has an Euler
    cycle. If it does, exhibit one.

    | ![graph21][g21] | ![graph22][g22] |
    |:---------------:|:---------------:|
    | Graph 21        | Graph 22        |

    [g21]: ./g21.png
    [g22]: ./g22.png

    i.  Graph 21 (above)
    i.  Graph 22 (above)

10. The following graph is continued to an arbitrary, finite depth. Does the
    graph contain an Euler cycle? If the answer is yes, describe one.

    | ![graph23][g23] |
    |:---------------:|
    | Graph 23        |

    [g23]: ./g23.png

10. When does the complete

    i.  graph $K_{n}$ contain an Euler cycle?
    i.  bipartite graph $K_{m,n}$ contain an Euler cycle?

11. A sports conference has 11 teams. It was proposed that each team play
    precisely one game against each of exactly nine other conference teams.
    Prove that this proposal is impossible to implement.

10. For graph 24 (below), find a path with no repeated edges from $d$ to $e$
    containing all the edges.

    | ![graph24][g24] |
    |:---------------:|
    | Graph 24        |

    [g24]: ./g24.png

-->

---

##  Miscellaneous exercises

*   Find a Hamiltonian cycle in $GK_{6}$.

*   True or false? _Dijkstra's_ algorithm finds the length of a shortest path in
    a connected, weighted graph even if some weights are negative. If true,
    prove it; otherwise, provide a counterexample.

<!-- Comment

*   Illustrate the previous exercise using the following graph.

    | ![graph25][g25] |
    |:---------------:|
    | Graph 25        |

    [g25]: ./g25.png

*   Tell whether each assertion is true or false.  If false, give a
    counterexample and if true, prove it.

    -   Let $G$ be a graph and let $v$ and $w$ be distinct vertices. If there is
        a path from $v$ to $w$, there is a simple path from $v$ to $w$.
    -   If a graph contains a cycle that includes all the edges, the cycle is an
        Euler cycle.

*   Let $G$ be a connected graph. Suppose that an edge $e$ is in a cycle. Show
    that $G$ with $e$ removed is still connected.

*   Show that the maximum number of edges in a simple, disconnected graph with
    $n$ vertices is $(n − 1)(n − 2)/2$.

-->


[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]

[pdf]: readme.pdf
[MAIN]: ../..

