# Homework 3

_Status:_ Final

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=2803960

**Due date:** Friday, April 24.

## Regular exercises

### Relations

> Section 3.3 in course textbook.

1.  Give examples of relations on $\{1,2,3,4\}$ having the properties listed
    below:

    i.  Reflexive, symmetric, and not transitive.
    i.  Not reflexive, not symmetric, and transitive
    i.  Symmetric and anti-symmetric.

1.  Determine whether each statement below true or false. If the statement is
    true, prove it; otherwise, give a counterexample.

    i.  If $R$ is transitive, then $R^{−1}$ is transitive.
    i.  If $R$ is reflexive, then $R^{−1}$ is reflexive.
    i.  If $R$ is symmetric, then $R^{−1}$ is symmetric.
    i.  If $R$ is symmetric, then $R^{−1}$ is anti-symmetric.

1.  How many non-empty relations are there on a set that contains

    i. 1 element?
    i. 2 elements?
    i. 3 elements?

1.  What is wrong with the following argument, which supposedly shows that any
    relation $R$ on $X$ that is symmetric and transitive is reflexive?

    > _Let_ $x\in X$. _Using symmetry, we have_ $(x,y)$ _and_ $(y,x)$ _both in_
    > $R$. _Since_ $(x,y), (y,x)\in R$, _by transitivity we have_ $(x,x)\in R$.
    > _Therefore, $R$ is reflexive._
    

### Equivalence Relations

> Section 3.4 in course textbook.

5.  Determine whether the given relation is an equivalence relation on
    $\{1,2,3,4,5\}$. If the relation is an equivalence relation, list the
    equivalence classes.

    i.  $\{(1,1), (2,2), (3,3), (4,4), (5,5), (1,3), (3,1)\}$
    i.  $\{(1,1), (2,2), (3,3), (4,4), (5,5), (1,3), (3,1), (3,4), (4,3)\}$
    i.  $\{(1,1), (2,2), (3,3), (4,4), (5,5), (1,5), (5,1), (3,5), (5,3), (1,3),
        (3,1)\}$

6.  Determine whether the given relation is an equivalence relation on the set
    of all people.

    i.  $\{(x,y)\,|\,x\text{ and }y\text{ are the same height}\}$
    i.  $\{(x,y)\,|\,x\text{ is taller than }y\}$
    i.  $\{(x,y)\,|\,x\text{ and }y\text{ have the same color hair}\}$

7.  Let $X = \{1, 2, 3, 4\}$. List the members of the equivalence relation on
    $X$ obtained via the partitions below.

    i.  Partition: $\{1,2\}$, $\{3,4\}$.
    i.  Partition: $\{1\}$, $\{2,4\}$, $\{3\}$.

8.  Let $X=\{1,2,3,4,5\}$, $Y=\{3,4\}$, and $C=\{1,3\}$. Define the relation $R$
    on $\mathcal{P}(X)$, the set of all subsets of $X$, as $(A,B)\in R$ if
    $A\cup Y=B\cup Y$. Show that $R$ is an equivalence relation.

9.  Let $X$, $Y$, $C$, and $R$ as in problem 8. List the elements of $[C]$, the
    equivalence class containing $C$.

10. If an equivalence relation has only one equivalence class, what must the
    relation look like?

11. Let $X=\{1,2,\dots,10\}$. Define a relation $R$ on $X\times X$ by
    $\big((a,b),(c,d)\big) \in R$ if $a + d = b + c$. Show that $R$ is an
    equivalence relation on $X \times X$.

12. Let $R_{1}$ and $R_{2}$ be equivalence relations on $X$. Show that
    $R_{1}\cap R_{2}$ is an equivalence relation on $X$.

13. Let $f$ be a function from $X$ to $Y$. Define a relation $R$ on $X$ by
    $(x,y)\in R$ if $f(x) = f(y)$. Show that $R$ is an equivalence relation on
    $X$.

14. Let $R$ be an equivalence relation on a set $A$. Define a function $f$ from
    $A$ to the set of equivalence classes of $A$ by the rule $f(x) = [x]$. When
    do we have $f(x) = f(y)$?

15. If $X$ and $Y$ are sets, we define the set $X$ to be equivalent to the set
    $Y$ if there is a one-to-one, onto function from $X$ to $Y$. Show that this
    set equivalence is an equivalence relation.

### Matrices of Relations

> Section 3.5 in course textbook.

16. For each of the relations $R$ below, find the matrix of the relation $R$
    from $X$ to $Y$ relative to the orderings given.

    i.  $R = \{(1,\delta), (2,\alpha), (2,\gamma), (3,\beta), (3,\gamma)\}$;
        ordering of $X:1,2,3;$ ordering of $Y:\alpha,\beta,\gamma,\delta$.
    i.  $R = \{(1,\delta), (2,\alpha), (2,\gamma), (3,\beta), (3,\gamma)\}$;
        ordering of $X:3,2,1;$ ordering of $Y:\gamma,\beta,\alpha,\delta$.

17. For each of the relations $R$ below, find the matrix of the relation $R$ on
    $X$ relative to the ordering given.

    i.  $R = \{(1,2), (2,3), (3,4), (4,5)\}$; ordering of $X:1,2,3,4,5$.
    i.  $R = \{(x,y)\,|\, x < y\}$; ordering of $X:1,2,3,4$.

18. Let $X = \{1,2,3\}$, $Y = \{x,y\}$, and $Z = \{a,b,c\}$. Consider the
    relations $R_{1}:X\to Y$, $R_{2}: Y\to Z$, given by
    \begin{align*}
      R_{1} & = \{(1,x), (1,y), (2,x), (3,x)\}; &  
      R_{2} & = \{(x,b), (y,b), (y,a), (y,c)\};  
    \end{align*}
    as well as the orderings:
    \begin{align*}
      X & :1, 2, 3; &  
      Y & :x, y; &  
      Z & :a, b, c.  
    \end{align*}

    i.  Find the matrix $A_{1}$ of the relation $R_{1}$ (relative to the given
        orderings).
    i.  Find the matrix $A_{2}$ of the relation $R_{2}$ (relative to the given
        orderings).
    i.  Find the matrix product $A_{1}A_{2}$.
    i.  Use the previous result to find the matrix of the relation $R_{2}\circ
        R_{1}$.
    i.  Use the previous result to find the relation $R_{2}\circ R_{1}$ (as a
        set of ordered pairs).

19. Let $X = Y = \{2,3,4,5\}$, and $Z = \{1,2,3,4\}$. Consider the
    relations $R_{1}:X\to Y$, $R_{2}: Y\to Z$, given by
    \begin{align*}
      R_{1} & = \{(x,y)\,|\, x\text{ divides } y\}; &  
      R_{2} & = \{(y,z)\,|\, y > z\};  
    \end{align*}
    as well as the orderings:
    \begin{align*}
      X & :5, 4, 3, 2; &  
      Y & :5, 4, 3, 2; &  
      Z & :4, 3, 2, 1.
    \end{align*}

    i.  Find the matrix $A_{1}$ of the relation $R_{1}$ (relative to the given
        orderings).
    i.  Find the matrix $A_{2}$ of the relation $R_{2}$ (relative to the given
        orderings).
    i.  Find the matrix product $A_{1}A_{2}$.
    i.  Use the previous result to find the matrix of the relation $R_{2}\circ
        R_{1}$.
    i.  Use the previous result to find the relation $R_{2}\circ R_{1}$ (as a
        set of ordered pairs).

20. How can we quickly determine whether a relation $R$ is a function by
    examining the matrix of $R$ (relative to some ordering)?

---

##  Miscellaneous exercises

*   How many non-empty relations are there on a set that contains 2020 elements?

*   Define a relation on $\mathbf{N}\times\mathbf{N}$ by $\big((p,q),(r,s)\big)
    \in R$ if $ps-qr = 0$. Show that $R$ is an equivalence relation on
    $\mathbf{N} \times \mathbf{N}$.

    i.  What is the equivalence class of $(1,1)$?
    i.  What is the equivalence class of $(1,2)$?
    i.  What can you say about the set $\mathbf{Q}^{+}$ of positive rational
        numbers?

    > _Hint:_
    >
    > Try using the notation $\frac{p}{q}$ to represent the ordered pair
    > $(p,q)$.

*   Let $\mathbf{Q}$ be the set of rational numbers (_i.e.,_ the set of all
    fractions). Let $\{a_{n}\} \subseteq \mathbf{Q}$ be a sequence of rational
    numbers. We say that $\{a_{n}\}$ is a **Cauchy sequence** if for each
    $\epsilon > 0$, there exists $N \in \mathbf{N}$, such that
    \begin{equation*}
      d(a_{n},a_{m}) < \epsilon,\text{ whenever } n,m \ge N;
    \end{equation*}
    where the _distance function_ $d:\mathbf{Q}\times\mathbf{Q}$ is given by
    $d(p,q) = | p - q |$.

    > _Note:_  
    > 
    > Convergent sequences in $\mathbf{Q}$ are Cauchy sequences.
    > However, the opposite is not always true. For example, the sequence
    > \begin{align*}
    >   a_{1} & = 3, &
    >   a_{2} & = 3.1, &
    >   a_{3} & = 3.14, &
    >   a_{4} & = 3.141, \\
    >   a_{5} & = 3.1415, &
    >   a_{6} & = 3.14159, &
    >   a_{7} & = 3.141592, &
    >   \cdots
    > \end{align*}
    > is a Cauchy Sequence that does not converge in $\mathbf{Q}$. It converges
    > in $\mathbf{R}$ to $\pi$.

    i.  Let $\mathcal{C}_{\mathbf{Q}}$ be the set of all Cauchy sequences in
        $\mathbf{Q}$.  Define a relation on $\mathcal{C}_{\mathbf{Q}}$ by
        \begin{equation*}
          \big(\{a_{n}\},\{b_{n}\}\big)\in R, \text{ if }
          \lim_{n\to\infty} |a_{n} - b_{n} | = 0.
        \end{equation*}
        Prove that $R$ is an equivalence relation.
    i.  Let $\mathbf{Q}^{*}$ be the set of equivalence classes of
        $\mathcal{C}_{\mathbf{Q}}$ obtained via $R$. Show that $\mathbf{Q}$ can
        be _embedded_ into $\mathbf{Q}^{*}$. That is, show there exists a
        _one-to-one_ function $f:\mathbf{Q}\to \mathbf{Q}^{*}$.

        > _Hint:_
        >
        > Are constant sequences Cauchy sequences? If so, try $f(q) =
        > \big[\{q_{n}\}\big]$, where $q_{n} = q$, for $n \ge 1$.

    _Fun fact:_
    
    The space $\mathbf{Q}^{*}$ can be turned into a _metric space_.
    That is, a function $d^{*}:\mathbf{Q}^{*}\times \mathbf{Q}^{*}\to\mathbf{R}$
    can be defined in such a way that it represents the _distance_ between two
    equivalence classes. With this _structure_ in place, _Cauchy sequences_ in
    $\mathbf{Q}^{*}$, are defined by replacing the distance function $d$ (above)
    with $d^{*}$. Unlike $\mathbf{Q}$, in this space $\mathbf{Q}^{*}$ all Cauchy
    sequences are now convergent. As it turns out, $\mathbf{Q}^{*}$ is
    equivalent to the set of real numbers $\mathbf{R}$.

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

