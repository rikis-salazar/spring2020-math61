#include <iostream> // std::cout, std::cin
#include <fstream>  // std::ofstream, std::ifstream
#include <string>   // std::string
#include <cstdlib>  // rand(), srand(...)
#include <ctime>    // time(...)
#include <set>      // std::set

int random_between(int a, int b){
    if ( a < b )
        return a + rand()%( b - a + 1 );
    return b + rand()%( a - b + 1 );
}

void generate_data_file( int hw_size ){
    const int MAX_SIZE = 5;
    std::ifstream fin;
    std::ofstream fout;
    fin.open("last-digits-SIDs.txt");
    fout.open("random-list.txt");

    std::string uid;
    while ( fin >> uid ){
        // Generate list with MAX_SIZE different elements between 1 and hw_size
        std::set<int> the_list;
        while ( the_list.size() < MAX_SIZE ){
            // The statement below sometimes produces a set of numbers that is
            // heavily biased towards one end of the list. E.g., for a set with
            // 20 problems, the random numbers could be 10, 14, 17, 18, 20.
            //
            //     the_list.insert( 1 + rand() % hw_size );
            //
            // Fix: make sure at least MAX_SIZE/2 problems are on either side of
            // the midpoint.
            int midpoint = hw_size/2;
            if ( the_list.size() < MAX_SIZE/2 ) {
                the_list.insert( random_between(1,midpoint) );
            }
            else{
                the_list.insert( random_between( 1 + midpoint, hw_size ) );
            }
        }

        fout << uid << ":\t";
        for ( auto x : the_list ){
            fout << x << " ";
        }
        fout << "\n";
    }

    fout.close();
    fin.close();
}

int main(){
    srand( static_cast<int>( time(0) ) );

    int number_problems;
    std::cout << "Enter size of homework: ";
    std::cin >> number_problems;
    generate_data_file( number_problems );

    return 0;
}
