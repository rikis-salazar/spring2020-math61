There is no random list for this assignment. You get to choose which two
problems you want typeset using \LaTeX.

Choose wisely. The idea is for you to select something appropriate to your
\LaTeX level. Ideally we want you to use symbols like $\binom{n}{r}$, $P(n,r)$,
or $\frac{n!}{n_{1}! n_{2}! \cdots n_{t}!}$. However, there will be no penalty
if you decide not to use symbols at all.
