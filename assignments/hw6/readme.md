# Homework 6

_Status:_ Final (although there might be some typos).

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=2803960

**Due date:** Friday, May 15.

## Regular exercises

### Recurrence Relations

> Section 7.1 in course textbook.

1.  Find a recurrence relation and initial conditions that generate a sequence
    that begins with the given terms.

    i.  3, 7, 11, 15, ...
    i.  3, 6, 9, 15, 24, 39, ...
    i.  1, 1, 2, 4, 16, 128, 4096, ...

1.  Assume that a person invests $2000 at 14 percent interest compounded
    annually. Let $A_{n}$ represent the amount at the end of $n$ years.

    i.  Find a recurrence relation for the sequence $A_{0}, A_{1}, \dots$ 
    i.  Find an initial condition for the sequence $A_{0}, A_{1}, \dots$ 
    i.  Find $A_{1}$, $A_{2}$, and $A_{3}$.
    i.  Find an explicit formula for $A_{n}$.

1.  Let $S_{n}$ denote the number of $n$-bit strings that do not contain the
    pattern `000`. Find a recurrence relation and initial conditions for the
    sequence $\{S_{n}\}$.

1.  For the following exercises, refer to the sequence $S$ where $S_{n}$ denotes
    the number of $n$-bit strings that do not contain the pattern `00`.

    i.  Find a recurrence relation and initial conditions for the sequence
        $\{S_{n}\}$.
    i.  Show that $S_{n} = f_{n+2}$, $n = 1, 2,\cdots$, where $f$ denotes the
        Fibonacci sequence.

1.  Write explicit solutions for the Tower of Hanoi puzzle for $n = 3, 4$.

1.  Suppose that we have $n$ dollars and that each day we buy either orange
    juice ($1), milk ($2), or beer ($2). If $R_{n}$ is the number of ways of
    spending all the money, show that $R_{n} = R_{n−1} + 2R_{n−2}$.

    > _Note:_ Order is taken into account. For example, there are 11 ways to
    > spend $4: MB, BM, OOM, OOB, OMO, OBO, MOO, BOO, OOOO, MM, BB.

1.  The sequence $g_{1},g_{2},\dots$ is defined by the recurrence relation
    $$g_{n} = g_{n−1} + g_{n−2} + 1,\qquad n \ge 3,$$
    and initial conditions $g_{1} = 1$, $g_{2} = 3$. By using mathematical
    induction or otherwise, show that
    $$g_{n} = 2f_{n+1} − 1,\qquad n \ge 1,$$
    where $f_{1},f_{2},\dots$ is the Fibonacci sequence.

1.  Define the sequence $t_{1}, t2, \dots$ by the recurrence relation
    $$t_{n} = t_{n−1}t_{n−2},\qquad n \ge 3,$$
    and initial conditions $t_{1} = 1$, $t_{2} = 2$. What is wrong with the
    following _"proof"_ that $t_{n} = 1$ for all $n \ge 1$?

    > _Basis Step:_ For $n = 1$, we have $t_{1} = 1$; thus, the Basis Step is
    > verified.
    >
    > _Inductive Step:_ Assume that $t_{k} = 1$ for $k < n$. We must prove that
    > $t_{n} = 1$. Now
    > \begin{align*}
    >   t_{n} & = t_{n−1}t_{n−2} \\
    >         &= 1\cdot 1\qquad\text{by the inductive assumption} \\
    >         & = 1.
    > \end{align*}
    > The Inductive Step is complete.

1.  The Lucas sequence $L_{1}, L_{2},\dots$ (named after Édouard Lucas, the
    inventor of the Tower of Hanoi puzzle) is defined by the recurrence relation
    $L_{n} = L_{n−1} + L_{n−2}$, $n \ge 3$, and the initial conditions $L_{1} =
    1$, $L_{2} = 3$.

    i.  Find the values of $L_{3}$, $L_{4}$, and $L_{5}$.
    i.  Show that
        $$L_{n+2} = f_{n+1} + f_{n+3},\qquad n \ge 1,$$
        where $f_{1},f_{2},\dots$ denotes the Fibonacci sequence.

---


### Solving Recurrence Relations

> Section 7.2 in course textbook.

10. For the recurrence relations below, first determine the order of the
    relation, then determine whether or not each of them is 

    * linear, or
    * linear homogeneous, or
    * linear with constant coefficients, or
    * linear homogeneous with constant coefficients.

    i.  $a_{n} = −3a_{n−1}$
    i.  $a_{n} = 2na_{n−2} - a_{n-1}$
    i.  $a_{n} = 2na_{n−1}$
    i.  $a_{n} = a_{n-1} + n$
    i.  $a_{n} = a_{n-1} + 1 + 2^{n-1}$

10. Solve the given recurrence relation for the initial conditions given.

    i.  $a_{n} = 2na_{n−1}$; $a_{0} = 1$.
    i.  $a_{n} = 6a_{n−1} − 8a_{n−2}$; $a_{0} = 1$, $a_{1} = 0$.
    i.  The Lucas sequence (see exercise 9).

10. Assume that the horse population of _The Kingdom of Rohan[^one]_ is 0 at
    time $n = 0$. Suppose that at time $n$, $100n$ horses are introduced into
    the kingdom and that the population increases 20 percent each year. Write a
    recurrence relation and an initial condition that define the horse
    population at time $n$ and then solve the recurrence relation.

    > _Note:_ The following formula may be of use:
    > \begin{equation}
    >   \sum_{k=1}^{n-1} kq^{k-1} = \frac{(n-1)q^{n} - nq^{n-1} + 1}{(q-1)^{2}}
    > \end{equation}

10. Solve the recurrence relation
    $$\sqrt{a_{n}} = \sqrt{a_{n−1}} + 2\sqrt{a_{n−2}}$$
    with initial conditions $a_{0} = a_{1} = 1$ by making the substitution
    $b_{n} = \sqrt{a_{n}}$.

10. Solve the recurrence relation
    $$a_{n} = \sqrt{\frac{a_{n-2}}{a_{n-1}}}$$
    with initial conditions $a_{0} = 8$, $a_{1} = \frac{1}{2\sqrt{2}}$ by taking
    the logarithm of both sides and making the substitution $b_{n} = \log
    a_{n}$.

    > _Note:_ Here $\log x$ stands for the _natural logarithm_; but any other
    > logarithm will work.

10. The equation
    $$a_{n} = f(n)a_{n−1} + g(n)a_{n−2}$$
    is called a _second-order, linear homogeneous_ recurrence relation. The
    coefficients $f(n)$ and $g(n)$ are not necessarily constant. Show that if
    $S$ and $T$ are solutions of the equation above, then $\alpha S + \beta T$
    is also a solution, for any choice of the numbers $\alpha$ and $\beta$.

---

##  Miscellaneous exercises

*   Use your knowledge of combinations to establish the following _well-known_
    identity
    $$(a+b)^{n} = \sum_{k=0}^{n} \binom{n}{k} a^{k}b^{n-k}.$$

    > _Hint:_
    >
    > Write $(a+b)^{n} = (a+b)(a+b)\cdots(a+b)$ and realize that any term in the
    > algebraic expression that results from developing the right hand side
    > consists of exactly $n$ factors. Some of them (say $k$ of them) will be
    > $a$'s while the rest will be $b$'s. Then count the number of terms in
    > every possible grouping.

*   By choosing specific values of $a$ and $b$ in the previous problem, prove
    the following identities:

    i.  $\binom{n}{0} + \binom{n}{1} + \binom{n}{2} + \cdots + \binom{n}{n} =
        2^{n}$ 
    i.  $2^{n}\binom{n}{0} - 2^{n-1}\binom{n}{1} + 2^{n-2}\binom{n}{2} + \cdots
        + (-1)^{n}\binom{n}{n} = 1$
    i.  $\binom{n}{0} - 2\binom{n}{1} + 4\binom{n}{2} - 8\binom{n}{3} + \cdots
        + (-2)^{n}\binom{n}{n} =
        \begin{cases}
        1 & \text{if }n\text{ is even,} \\
        -1 & \text{if }n\text{ is odd.}
        \end{cases}$ 

*   For the following exercises, refer to the sequence $S_{1},S_{2},\dots$ where
    $S_{n}$ denotes the number of $n$-bit strings that do not contain the
    pattern `010`.

    i.  Compute $S_{1}$, $S_{2}$, $S_{3}$, and $S_{4}$.
    i.  By considering the number of $n$-bit strings that do not contain the
        pattern `010` that have no leading `0`’s (i.e., that begin with `1`);
        that have one leading `0` (i.e., that begin `01`); that have two leading
        `0`’s; and so on, derive the recurrence relation
        $$S_{n} = S_{n−1} + S_{n−3} + S_{n−4} + S_{n−5} +···+ S_{1} + 3.$$ 
    i.  By replacing $n$ by $n − 1$ in the equation above, write a formula for
        $S_{n−1}$. Subtract the formula for $S_{n−1}$ from the formula for
        $S_{n}$ and use the result to derive the recurrence relation
        $$S_{n} = 2S_{n−1} − S_{n−2} + S_{n−3}.$$

*   Suppose that both roots of 
    $$t^{2} − c_{1}t − c_{2} = 0$$
    are equal to $r$, and suppose that $a_{n}$ satisfies
    $$a_{n} = c_{1}a_{n−1} + c_{2}a_{n−2},\qquad a_{0} = C_{0},\qquad a_{1} =
    C_{1}.$$
    Show that there exist constants $\alpha$ and $\beta$ such that
    $$a_{n} = \alpha r^{n} + \beta nr^{n},\qquad n = 0,1,\dots$$


[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]

[^one]: Of course this is a rather silly assumption. As pretty much everybody
  knows, _Rohan_ (or _Rochand_) is the home of the _Rohirrim_, and the meaning
  of the word is _Horse-country_. Thus, it would be impossible to have a
  population of 0 horses if the name was already established.

[pdf]: readme.pdf
[MAIN]: ../..

