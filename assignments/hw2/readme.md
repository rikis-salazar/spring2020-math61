# Homework 2

_Status:_ Final.

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=2803960

**Due date:** Friday, April 17.

## Regular exercises

### Functions

> Section 3.1 in course textbook.

1.  Given
    $$g = \{(1,b), (2,c), (3,a)\},$$
    a function from $X = \{1, 2, 3\}$ to $Y = \{a, b, c, d\}$, and
    $$f = \{(a,x), (b,x), (c,z), (d,w)\},$$
    a function from $Y$ to $Z = \{w, x, y, z\}$, write $f \circ g$ as a set of
    ordered pairs and draw the arrow diagram of $f \circ g$.

1.  Let $f$ and $g$ be functions from the positive integers to the positive
    integers defined by the equations
    $$f(n) = 2n +1,\qquad g(n) = 3n - 1.$$
    Find the compositions $f\circ f$, $g\circ g$, $f\circ g$, and $g\circ f$.

1.  Given
    $$f = \{(a,b), (b,a), (c,b)\},$$
    a function from $X = \{a, b, c\}$ to $X$:

    i.  Write $f\circ f$ and $f\circ f\circ f$ as sets of ordered pairs.
    i.  Define
        $$f^{n} = f\circ f\circ \cdots\circ f$$
        to be the $n$-fold composition of $f$ with itself. Write $f^{9}$,
        $f^{623}$, and $f^{2020}$ as sets of ordered pairs.

1.  Let $f$ be the function from $X = \{0, 1, 2, 3, 4, 5\}$ to $X$ defined by
    $$f(x) = 4x\,\text{mod}\,6.$$
    Write $f$ as a set of ordered pairs and draw the arrow diagram of $f$. Is
    $f$ one-to-one? Is $f$ onto?

1.  Is the formula $\lceil x+y \rceil = \lceil x \rceil + \lceil y \rceil$ true
    for all real numbers? Prove it, or give a counter-example.

### Sequences & strings

> Section 3.2 in course textbook.

6.  Determine whether the sequence $s$ defined by $s_{n} = 2^{n} − n^{2}$ is
    _increasing_, _decreasing_, _nonincreasing_, or _nondecreasing_, for the
    given domain $D$.

    i.  $D=\{1,2,3,4\}$
    i.  $D=\{ n\,|\, n \in\mathbf{Z},\,n\ge 2020\}$

6.  For the sequence $t$ defined by $t_{n} = 2n -1$, $n\ge 1$.

    i.  Find $t_{3}$, $t_{7}$, $t_{100}$, and $t_{2020}$.
    i.  Find $\prod\limits_{i=3}^{6} t_{i}$, and $\sum\limits_{i=3}^{7} t_{i}$.

6.  For the sequence $w$ defined by $w_{n} = \frac{1}{n} - \frac{1}{n+1}$, $n
    \ge 1$.

    i.  Find $\sum\limits_{i=1}^{3} w_{i}$, and $\sum\limits_{i=1}^{10}w_{i}$.
    i.  Find $\sum\limits_{i=1}^{2020} w_{i}$.

6.  Let $r$ be the sequence defined by $r_{n} = 3 \cdot 2^{n} -4 \cdot 5^{n}$,
    $n\ge 0$.

    i.  Find $r_{0}$, $r_{1}$, $r_{2}$, and $r_{3}$.
    i.  Find a formula for $r_{n-1}$, and $r_{n-2}$.
    i.  Prove that the sequence $\{r_{n}\}$ satisfies
        $$r_{n} = 7 r_{n-1} - 10 r_{n-2},\qquad n\ge 2.$$

6.  Rewrite the sum
    $$\sum_{i=1}^{n} i^{2} r^{n-i}$$
    replacing the index $i$ by $k$, where $i=k+1$.

6.  Compute the given quantity using the strings
    $$\alpha = baab,\qquad \beta = caaba,\qquad \gamma = bbab.$$

    i.  $\alpha\beta$
    i.  $\beta\alpha$
    i.  $\alpha\alpha$
    i.  $\beta\beta$
    i.  $|\alpha\beta|$
    i.  $|\beta\alpha|$

6.  List all strings over $X = \{0, 1\}$ of ...

    i. lenght 2.
    i. lenght 2 or less.
    i. lenght 3.
    i. lenght 3 or less.
    i. lenght 3 without two consecutive zeros.

6.  Find all substrings of the string $babc$.

6.  Let $X =\{a, b\}$. A _palindrome over_ $X$ is a string $\alpha$ for which
    $\alpha = \alpha^{R}$ (i.e., a string that reads the same forward and
    back-ward). An example of a palindrome over $X$ is $bbaabb$. Define a
    function from $X^{∗}$ to the set of palindromes over $X$ as $f(\alpha) =
    \alpha\alpha^{R}$. Is $f$ one-to-one? Is $f$ onto? Prove your answers.

6.  Let $L$ be the set of all strings, including the null string, that can be
    constructed by repeated application of the following rules:

    *   If $\alpha\in L$, then $a\alpha b\in L$, and $b\alpha a\in L$.
    *   If $\alpha\in L$ and $\beta\in L$, then $\alpha\beta\in L$.

    For example, $ab$ is in $L$, for if we take $\alpha = \lambda$, then
    $\alpha\in L$ and the first rule states that $ab = a\alpha b \in L$.
    Similarly, $ba\in L$. As a final example, $abba$ is in $L$, for if we take
    $\alpha = ab$ and $\beta = ba$, then $\alpha\in L$ and $\beta\in L$; by the
    second rule, $abba = \alpha\beta\in L$.

    i.  Show that $aaabbb$ is in $L$.
    i.  Show that $baabab$ is in $L$.
    i.  Show that $aab$ is not in $L$.

### Relations

> Section 3.3 in course textbook.

16. Draw the _digraph_ of the following relations:

    i.  $R = \{(1,2), (2,3), (3,4), (4,1)\}$ on $\{1, 2, 3, 4\}$.
    i.  $R = \{(1,2), (2,1), (3,3), (1,1), (2,2)\}$ on $\{1, 2, 3\}$.

16. Let $R$ be the relation on the set $\{1,2,3,4,5\}$ defined by the rule
    $(x,y)\in R$ if 3 divides $x-y$.

    i.  List the elements of $R$.
    i.  List the elements of $R^{-1}$.
    i.  Repeat the previous two parts when $R$ is defined by the rule $(x,y)\in
        R$ if $x + y \le 6$.

16. For the given relation $R$ on the set of positive integers, determine if it
    is _reflexive_, _symmetric_, _antisymmetric_, _transitive_, and/or a
    _partial order_.

    i.  $(x,y)\in R$ if 3 divides $x-y$.
    i.  $(x,y)\in R$ if $x \ge y$.

16. Let $X$ be a nonempty set. Define a relation on $\mathcal{P}(X)$, the power
    set of $X$, as $(A,B)\in R$ if $A \subseteq B$. Is this relation
    _reflexive_, _symmetric_, _antisymmetric_, _transitive_, and/or a _partial
    order_?

16. Let $R_{1}$ and $R_{2}$ be the relations on $\{1,2,3,4\}$ given by
    \begin{align}
      R_{1} & = \{(1,1), (1,2), (3,4), (4,2)\} \\
      R_{2} & = \{(1,1), (2,1), (3,1), (4,4), (2,2)\}.
    \end{align}
    List the elements of $R_{1}\circ R_{2}$ and $R_{2}\circ R_{1}$.

---

##  Miscellaneous exercises

-   A store offers a (fixed, nonzero) percentage off the price of certain items.
    A coupon is also available that offers a (fixed, nonzero) amount off the
    price of the same items. The store will honor both discounts. Show that
    regardless of the price of an item, the percentage off the price, and amount
    off the price, it is always cheapest to use the coupon first.

-   Let $f:\mathbf{N}\to\mathbf{Z}$ be the function defined by the formula
    $$f(n) = (-1)^{n+1} \Big\lfloor \frac{n}{2} \Big\rfloor.$$
    Is $f$ _one-to-one_? Is it _onto_? Is it a _bijection_?

-   Given
    $$f = \{(x, x^{2})\,|\, x\in X\},$$
    a function from $X = \{−5, −4, . . . , 4, 5\}$ to the set of integers, write
    $f$ as a set of ordered pairs and draw the arrow diagram of $f$. Is $f$
    one-to-one or onto?

-   Let $a_{n} = n^{2} - 3n + 3$, for $n=1, 2, \dots$ Find

    i.  $\sum\limits_{n=1}^{k} a_{2020}$
    i.  $\sum\limits_{n=1}^{2020} a_{n}$
    i.  $\sum\limits_{n=1}^{k} a_{n}$

-   Use induction to prove that
    $$\sum \frac{1}{n_{1}\cdot n_{2} \cdots n_{k}} = n,$$
    for all $n\ge 1$, where the sum is taken over all nonempty subsets
    $\{n_{1},n_{2},\dots,n_{k}\}$ of $\{1,2,\dots,n\}$.

-   Find all substrings of the string $aabaabb$.

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

