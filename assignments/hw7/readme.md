# Homework 7

_Status:_ Final (although there might be some typos).

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=2803960

**Due date:** Friday, May 22.

## Regular exercises

### Introduction to Graphs

> Section 8.1 in course textbook.

1.  In a tournament, the _Spiders_ beat the _Balrogs_ once, the _Trolls_ beat
    the _Wargs_ once, the _Spiders_ beat the _Trolls_ twice, the _Balrogs_ beat
    the _Wargs_ once, and the _Balrogs_ beat the _Spiders_ once.

    In the following exercises, use a graph to model the tournament. The teams
    are the vertices. Describe the kind of graph used (e.g., undirected graph,
    directed graph, simple graph).

    i.  There is an edge between teams if the teams played.
    i.  There is an edge from team $t_{i}$ to team $t_{j}$ if $t_{i}$ beat
        $t_{j}$ at least one time.
    i.  There is an edge between teams for each game played.
    i.  There is an edge from team $t_{i}$ to team $t_{j}$ for each victory of
        $t_{i}$ over $t_{j}$.

1.  Show that each graph below has a path from $a$ to $a$ that passes through
    each edge exactly one time by finding such a path by inspection.

    | ![graph01][g01] | ![graph02][g02] | ![graph3][g03] |
    |:---------------:|:---------------:|:--------------:|
    | Graph 1         |  Graph 2        | Graph 3        |

    [g01]: ./g01.png
    [g02]: ./g02.png
    [g03]: ./g03.png

    1.  Graph 1 (above)
    1.  Graph 2 (above)
    1.  Graph 3 (above)

1.  Explain why none of the graphs below has a path from $a$ to $a$ that passes
    through each edge exactly one time.

    | ![graph04][g04] | ![graph05][g05] | ![graph06][g06] |
    |:---------------:|:---------------:|:---------------:|
    | Graph 4         | Graph 5         | Graph 6         |

    [g04]: ./g04.png
    [g05]: ./g05.png
    [g06]: ./g06.png

    1.  Graph 4 (above)
    1.  Graph 5 (above)
    1.  Graph 6 (above)

1.  For each graph $G = (V, E)$ below, find $V$, $E$, all parallel edges, all
    loops, all isolated vertices, and tell whether $G$ is a simple graph. Also,
    tell on which vertices edge $e_{1}$ is incident.

    | ![graph07][g07] | ![graph08][g08] | ![graph09][g09] |
    |:---------------:|:---------------:|:---------------:|
    | Graph 7         | Graph 8         | Graph 9         |

    [g07]: ./g07.png
    [g08]: ./g08.png
    [g09]: ./g09.png

1.  Find a formula for the number of edges in $K_{n}$.

1.  State which graphs below are bipartite graphs. If the graph is bipartite,
    specify the disjoint vertex sets.

    | ![graph10][g10] | ![graph11][g11] |
    |:---------------:|:---------------:|
    | Graph 10        | Graph 11        |

    [g10]: ./g10.png
    [g11]: ./g11.png

    i. Graph 7 (above)
    i. Graph 8 (above)
    i. Graph 10 (above)
    i. Graph 11 (above)

1.  Find a formula for the number of edges in $K_{m,n}$.

1.  In the following exercises, find a path of minimum length from $v$ to $w$ in
    graph 12 (below) that passes through each vertex exactly one time.

    | ![graph12][g12] |
    |:---------------:|
    | Graph 12        |

    [g12]: ../../notes/graph2.png

    i.  $v = b$, $w = e$.
    i.  $v = a$, $w = b$.
    i.  $v = c$, $w = d$.

1.  In the following graph the vertices represent cities and the numbers on the
    edges represent the costs of building the indicated roads. Find a
    least-expensive road system that connects all the cities.

    | ![graph13][g13] |
    |:---------------:|
    | Graph 13        |

    [g13]: ./g13.png

1.  Let $\mathcal{G}$ denote the set of simple graphs $G = (V, E)$, where $V =
    \{1, 2,\cdots, n\}$ for some $n \in \mathbf{N}$. Define a function $f$ from
    $\mathcal{G}$ to $\mathbf{N}\cup\{0\}$ by the rule $f(G) = |E|$. Is $f$
    one-to-one? Is $f$ onto? Explain.

---


### Paths and Cycles

> Section 8.2 in course textbook.

11. In the following exercises, tell whether the given path in the graph 14 is

    *   A simple path
    *   A cycle
    *   A simple cycle

    | ![graph14][g14] |
    |:---------------:|
    | Graph 14        |

    [g14]: ./g14.png

    i.  $(a, d, c, d, e)$
    i.  $(e, d, c, b)$
    i.  $(b, c, d, e, b, b)$
    i.  $(b, c, d, a, b, e, d, c, b)$
    i.  $(d, c, d)$

10. In the following exercises, draw a graph having the given properties or
    explain why no such graph exists.

    i.  Six vertices each of degree 3.
    i.  Four vertices each of degree 1.
    i.  Five vertices each of degree 3.
    i.  Six vertices; four edges.
    i.  Four edges; four vertices having degrees 1, 2, 3, 4.
    i.  Simple graph; six vertices having degrees 1, 2, 3, 4, 5, 5.

10. For graph 15 (below), find all

    * the simple cycles; and
    * the simple paths from $a$ to $e$.

    | ![graph15][g15] |
    |:---------------:|
    | Graph 15        |

    [g15]: ./g15.png

10. Find the degree of each vertex for the following graphs.

    | ![graph16][g16] | ![graph17][g17] |
    |:---------------:|:---------------:|
    | Graph 16        | Graph 17        |

    [g16]: ./g16.png
    [g17]: ./g17.png

10. In the following exercises, find all subgraphs having at least one vertex of
    the specified graph.

    | ![graph18][g18] | ![graph19][g19] | ![graph20][g20] |
    |:---------------:|:---------------:|:---------------:|
    | Graph 18        | Graph 19        | Graph 20        |

    [g18]: ./g18.png
    [g19]: ./g19.png
    [g20]: ./g20.png

    i.  Graph 18 (above)
    i.  Graph 19 (above)
    i.  Graph 20 (above)

10. In the following exercises, decide whether the specified graph has an Euler
    cycle. If it does, exhibit one.

    | ![graph21][g21] | ![graph22][g22] |
    |:---------------:|:---------------:|
    | Graph 21        | Graph 22        |

    [g21]: ./g21.png
    [g22]: ./g22.png

    i.  Graph 21 (above)
    i.  Graph 22 (above)

10. The following graph is continued to an arbitrary, finite depth. Does the
    graph contain an Euler cycle? If the answer is yes, describe one.

    | ![graph23][g23] |
    |:---------------:|
    | Graph 23        |

    [g23]: ./g23.png

10. When does the complete

    i.  graph $K_{n}$ contain an Euler cycle?
    i.  bipartite graph $K_{m,n}$ contain an Euler cycle?

11. A sports conference has 11 teams. It was proposed that each team play
    precisely one game against each of exactly nine other conference teams.
    Prove that this proposal is impossible to implement.

10. For graph 24 (below), find a path with no repeated edges from $d$ to $e$
    containing all the edges.

    | ![graph24][g24] |
    |:---------------:|
    | Graph 24        |

    [g24]: ./g24.png

---

##  Miscellaneous exercises

*   Let $G$ be a connected graph with four vertices $v_{1}$, $v_{2}$, $v_{3}$,
    and $v_{4}$ of odd degree. Show that there are paths with no repeated edges
    from $v_{1}$ to $v_{2}$ and from $v_{3}$ to $v_{4}$ such that every edge in
    $G$ is in exactly one of the paths.

*   Illustrate the previous exercise using the following graph.

    | ![graph25][g25] |
    |:---------------:|
    | Graph 25        |

    [g25]: ./g25.png

*   Tell whether each assertion is true or false.  If false, give a
    counterexample and if true, prove it.

    -   Let $G$ be a graph and let $v$ and $w$ be distinct vertices. If there is
        a path from $v$ to $w$, there is a simple path from $v$ to $w$.
    -   If a graph contains a cycle that includes all the edges, the cycle is an
        Euler cycle.

*   Let $G$ be a connected graph. Suppose that an edge $e$ is in a cycle. Show
    that $G$ with $e$ removed is still connected.

*   Show that the maximum number of edges in a simple, disconnected graph with
    $n$ vertices is $(n − 1)(n − 2)/2$.

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]

[pdf]: readme.pdf
[MAIN]: ../..

