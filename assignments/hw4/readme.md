# Homework 4

_Status:_ Final.

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=2803960

**Due date:** Friday, May 1.

## Regular exercises

### Counting (basic principles)

> Section 6.1 in course textbook.

1.  Answer the following questions.

    i.  A man has eight shirts, four pairs of pants, and five pairs of shoes.
        How many different outfits are possible?
    i.  The _Braille_ system of representing characters was developed early in
        the nineteenth century by _Louis Braille_. The characters, used by the
        blind, consist of raised dots. The positions for the dots are selected
        from two vertical columns of three dots each. At least one raised dot
        must be present. How many distinct Braille characters are possible?
    i.  Two dice are rolled, one blue and one red. How many outcomes are
        possible?

2.  Consider the alphabet $X=\{A,B,C,D,E,F\}$.

    > _Errata:_ the original set of problems was missing the length of the
    > strings (see boldface words below).

    i.  How many strings **of length 5** begin with the letter F and end with
        the letter A if letters are not allowed to be repeated?
    i.  How many strings **of length 5** begin with the letter F and end with
        the letter A if repetitions of letters are allowed?
    i.  How many strings **of length 5** begin with the letter F and do not end
        with EB in that order? Assume repetitions are not allowed.

3.  Use the addition principle to solve the problems below.

    i.  A committee composed of _Marley_, _Tonatiuh_, _Mateo_, and _Leandro_ is
        to select a president and secretary. How many selections are there in
        which _Tonatiuh_ is president or not an officer?

    i.  A committee composed of _Marley_, _Tonatiuh_, _Mateo_, and _Leandro_ is
        to select a president and secretary. How many selections are there in
        which _Mateo_ is president or secretary?

4.  In the following exercises, two dice are rolled, one blue and one red.

    i.  How many outcomes give the sum of 7 or the sum of 11?
    i.  How many outcomes have exactly one die showing 2?
    i.  How many outcomes have at least one die showing 2?

5.  In the exercises below, a six-person committee composed of _Abigail_,
    _Beatriz_, _Citlali_, _Daneli_, _Elisa_, and _Frida_ is to select a
    chairperson, secretary, and treasurer.

    i.  How many selections exclude _Citlali_?
    i.  How many selections are there in which _Daneli_ is an officer and
        _Frida_ is not an officer?
    i.  How many selections are there in which _Beatriz_ is either chairperson
        or treasurer?

6.  Answer the following questions about relations on a finite set.

    i.  How many symmetric relations are there on an $3$-element set?
    i.  How many antisymmetric relations are there on an $3$-element set?
    i.  How many symmetric relations are there on an $4$-element set?
    i.  How many antisymmetric relations are there on an $4$-element set?

7.  A six-person committee composed of _Abigail_, _Beatriz_, _Citlali_,
    _Daneli_, _Elisa_, and _Frida_ is to select a chairperson, secretary, and
    treasurer. How many selections are there in which either _Beatriz_ is
    chairperson or _Abigail_ is secretary or both?

8.  How many integers from 1 to 10,000, inclusive, are multiples of 5 or 7 or
    both?

---

### Permutations and combinations

> Section 6.2 in course textbook.

9.  Answer the following questions.

    i.  List the permutations of $a$, $b$, $c$, $d$.
    i.  How many permutations are there of 11 distinct objects?
    i.  How many 5-permutations are there of 11 distinct objects?

10. Answer the following questions.

    i.  In how many ways can five distinct _Dwarves_ and five distinct _Elves_
        wait in line?
    i.  In how many ways can five distinct _Dwarves_ and five distinct _Elves_
        be seated at a circular table?

11. Let $X=\{w,x,y,z\}$. Compute the number of 3-combinations of $X$. Verify
    this number by listing all of the 3-combinations of $X$.

12. In how many ways can we select a committee of four _Uruk-hai_, three
    _Haradrim_, and two _Nazgûl_ from a group of 10 distinct _Uruk-hai_, 12
    distinct _Haradrim_, and four distinct _Nazgûl_?

13. In the following exercises, find the number of (unordered) five-card poker
    hands, selected from an ordinary 52-card deck, having the properties
    indicated.

    i.  Containing four aces.
    i.  Containing four of a kind, that is, four cards of the same denomination.
    i.  Containing all spades.

14. In the following exercises, determine the number of strings that can be
    formed by ordering the letters given.

    i.  SALESPERSONS
    i.  SUPERCALIFRAGILÍSTICO
    i.  OTORRINOLARINGÓLOGO

        > _Note:_ Assume letters with accents are _distinguishable_ from letters
        > without accents. _E.g.:_ Á is not the same letter as A.

15. Assume you have three piles of identical red, blue, and green balls where
    each pile contains at least 10 balls. In how many ways can 10 balls be
    selected if exactly one red ball must be selected?

    ---

##  Miscellaneous exercises

*   How many symmetric relations are there on an $n$-element set?

*   How many antisymmetric relations are there on an $n$-element set?

*   Prove the _Inclusion-Exclusion Principle_ for three finite sets:
    $$|X\cup Y\cup Z| = |X| + |Y| + |Z| − |X\cap Y| − |X\cap Z| − |Y\cap Z| +
    |X\cap Y\cap Z|.$$

    > _Hint:_ Write the Inclusion-Exclusion Principle for two finite sets as
    > $$|A\cup B| = |A| + |B| − |A\cap B|$$
    > and let $A = X$ and $B = Y\cup Z$.

*   In a group of 191 students, 10 are taking French, business, and music; 36
    are taking French and business; 20 are taking French and music; 18 are
    taking business and music; 65 are taking French; 76 are taking business; and
    63 are taking music. Use the Inclusion-Exclusion Principle for three finite
    sets (see previous exercise) to determine how many students are not taking
    any of the three courses.

*   Use the Inclusion-Exclusion Principle for three finite sets (see previous
    exercises) to compute the number of integers between 1 and 10,000,
    inclusive, that are multiples of 3 or 5 or 11 or any combination thereof.

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

