# Homework 1

_Status:_ Final (although there might be some typos).

> Please reach out to me via the [CCLE forum][df] for corrections and/or
> clarifications about statements found in this document.

[df]: https://ccle.ucla.edu/mod/forum/view.php?id=2803960

**Due date:** Wednesday, April 8.

## Regular exercises

### Mathematical Induction

> Section 2.4 in course textbook.

For the problems below, use induction to verify that each equation is true for
every positive integer _n_.

1.  $1 + 3 + 5 + \cdots + (2n-1) = n^{2}$.

1.  $1 + 2^{2} + 3^{2} + \cdots + n^{2} = \frac{n(n+1)(2n+1)}{6}$.

1.  $\frac{1}{2^{2}-1} + \frac{1}{3^{2}-1} + \dots + \frac{1}{(n+1)^{2}-1}
    = \frac{3}{4} - \frac{1}{2(n+1)} - \frac{1}{2(n+2)}$.

Use induction to prove the following statements:

4.  $7^{n}-1$ is divisible by 6, for all $n\ge 1$.

4.  $11^{n}-6$ is divisible by 5, for all $n\ge 1$.

Use induction to verify the following inequalities:

6.  $\frac{1}{2n} \le
    \frac{1\,\cdot\,3\,\cdot\,5\,\cdots\,(2n-1)}
    {2\,\cdot\,4\,\cdot\,6\,\cdots\,(2n)}$, for $n=1, 2, \dots$

6.  $2n + 1 \le 2^{n}$, for $n = 3, 4, \dots$

6.  $2^{n} \ge n^{2}$, for $n = 4, 5, \dots$

6.  Use the geometric sum formula to prove that
    $$r^{0} + r^{1} + \cdots + r^{n} < \frac{1}{1-r}$$
    for all $n > 0$ and $0<r<1$.

This exercise should help you complete _Example 3_ from our mathematical
induction lecture.

10. A $2^{n}\times2^{n}$ _L-shape_, $n\ge0$, is a figure of the form

    | ![_L-shape_][fig1] |
    |:------------------:|

    with no missing squares. Show that if $n>0$, any $2^{n}\times2^{n}$ L-shape
    can be tiled with trominoes.

    [fig1]: ./l-shape.png


### Sets & functions

> Sections 1.1 & 3.1 in course textbook.

11. Let the _universal set[^one]_ be the set $U = \{1,2,3,\dots,10\}$. Let $A =
    \{1,4,7,10\}$, $B = \{1,2,3,4,5\}$, and $C = \{2,4,6,8\}$. List the elements
    of each set.

    a)  $A \cup B - C$
    a)  $\overline{A \cap B} \cup C$
    a)  $(A \cup B) - (C - B)$

    [^one]: The _universe_, or _universal set_, is the set of all elements
    under discussion for possible membership in a set.

11. Answer the following questions:

    a)  What is the cardinality of $\emptyset$?
    a)  What is the cardinality of $\{\emptyset\}$?
    a)  What is the cardinality of $\{a,b,a,c\}$?
    a)  What is the cardinality of
        $\big\{ \{a\}, \{a,b\}, \{a,c\}, a, b \big\}$?

11. Carefully show that $A \neq B$.

    a)  $A = \{1,2\}$, $B = \{x\,|\,x^{3}-2x^{2}-x+2 = 0\}$.
    a)  $A = \{1,3,5\}$, $B = \{ n \in\mathbb{Z}\,|\,n>0 \text{ and } n^{2} - 1 \le n \}$.

11. Carefully show that $A$ is not a subset of $B$.

    a)  $A = \{1,2,3\}$, $B = \{1,2\}$.
    a)  $A = \{1,2,3\}$, $B = \emptyset$.

11. A television poll of 151 persons found that 68 watched _Law and Disorder_;
    61 watched _Twenty-five_; 52 watched _The Tenors_; 16 watched both _Law and
    Disorder_ and _Twenty-five_; 25 watched both _Law and Disorder_ and _The
    Tenors_; 19 watched both _Twenty-five_ and _The Tenors_; and 26 watched
    none of these shows. How many persons watched all three shows?

11. Let $X = \{1,2\}$, $Y = \{a\}$, and $Z = \{\alpha,\beta\}$. List the
    elements of each of the following sets.

    a)  $X\times Y\times Z$
    a)  $X\times X\times X$
    a)  $Z\times Y\times X$

11. Determine whether each set below is a function from $X = \{1,2,3,4\}$ to $Y
    = \{a,b,c,d\}$. If it is a function, find its domain and range, draw its
    arrow diagram, and determine if it is one-to-one, onto, or both.

    a)  $\{(1,a), (2,a), (3,c), (4,b)\}$
    a)  $\{(1,c), (2,a), (3,b), (4,c), (2,d)\}$
    a)  $\{(1,c), (2,d), (3,a), (4,b)\}$

11. Determine whether each function below is one-to-one, onto, or both. Prove
    your answers. The domain of each function is the set of all integers. The
    codomain of each function is also the set of all integers.

    a)  $f(n) = n + 1$
    a)  $f(n) = n^{2} - 1$
    a)  $f(n) = n^{3}$

11. Write the definition of _one-to-one_ using logical notation (_i,e.,_ use
    $\forall$, $\exists$, etc.).

11. Write the definition of _onto_ using logical notation (_i,e.,_ use
    $\forall$, $\exists$, etc.).

---

##  Miscellaneous exercises

-   Use induction to prove the following identity
    $$1^{3} + 2^{3} + 3^{3} + \cdots + n^{3} = \Big(\frac{n(n+1)}{2}\Big)^{2}.$$

-   A _3D-septomino_ is a three-dimensional $2\times 2\times 2$ cube with one
    $1\times 1\times 1$ corner cube removed. A deficient cube is a $k\times
    k\times k$ cube with one $1\times 1\times 1$ cube removed.

    Prove that a $2^{n}\times 2^{n}\times 2^{n}$ deficient cube can be tiled by
    3D-septominoes.

-   Let $\mathcal{P}(X)$ denote the _power set_ of $X$. Answer the following
    questions:

    *   List the members of $\mathcal{P}\big(\{a,b\}\big)$. Which are proper
        subsets of $\{a,b\}$?
    *   If $X$ has 10 members, how many members does $\mathcal{P}(X)$ have? How
        many proper subsets does $X$ have?
    *   If $X$ has $n$ members, how many members does $\mathcal{P}(X)$ have? How
        many proper subsets does $X$ have?

-   Let $\mathbb{N}$ denote the set of natural numbers. Prove that the function
    $f$ from $\mathbb{N}\times\mathbb{N}$ defined by $f(m,n) = 2^{m}3^{n}$ is
    _one-to-one_ but not _onto_.

-   Use [_De Morgan’s laws of logic_][dmll] to negate the definition of
    _one-to-one_.

-   Use [_De Morgan’s laws of logic_][dmll] to negate the definition of
    _onto_.

    [dmll]: https://en.wikipedia.org/wiki/De_Morgan%27s_laws

---

[Click here to access a `.pdf` version of this assignment][pdf]  

---

[Return to main course website][MAIN]


[pdf]: readme.pdf
[MAIN]: ../..

