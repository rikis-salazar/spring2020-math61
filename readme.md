# MATH 61: Introduction to Discrete Structures (Spring 2020)

This is to be considered the class website. Here you will find material related
to the concepts we discuss during lecture.

## Sections

*   [The class syllabus][syllabus] ([`.pdf` version][syllabus-pdf])
*   [Homework assignments][hw]
*   [Lecture recordings][lec]
*   [Lecture notes][notes]
*   [**Acessing Zoom meetings (no waiting room)**][zoom]

[syllabus]: syllabus/
[syllabus-pdf]: syllabus/readme.pdf
[hw]: assignments/
[lec]: https://ccle.ucla.edu/course/view.php?id=86664&section=3
[notes]: notes/
[zoom]: zoom-no-waiting-room/

<!-- This is a comment.
[qs]: https://www.kualo.co.uk/404
-->


## Additional resources

*   [The CCLE class website][CCLE]: this will be used class announcements, and
    to host lecture recordings.

[CCLE]: https://ccle.ucla.edu/course/view/20S-MATH61-2
