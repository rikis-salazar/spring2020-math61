# Accessing Zoom meetings (waiting room bypass)

> **TL;DR** -  If you use a _free_ account to _attend_ UCLA Zoom lectures that
> have setup a waiting room, you need somebody to grant you permission to
> _enter_ the meeting.
>
> Avoid this hassle by signing in to Zoom with your UCLA credentials.

---

## Method 1: directly via web browser

Simply follow these steps:

1.  Go to [`https://ucla.zoom.us/`](https://ucla.zoom.us/); then click on the
    _"LOGIN TO ZOOM"_ legend (see picture).

    | ![UCLA Zoom login][fig1] |
    |:------------------------:|
    |_Fig 1: The UCLA Zoom login portal._ |

1.  In the next screen you will prompted for your UCLA credentials (_e.g.,_ the
    login/password combo you use to access MyUCLA, or CCLE).

    | ![UCLA login][fig2] |
    |:------------------------:|
    |_Fig 2: The UCLA login portal._ |

1.  Open a new browser window, then enter the zoom meeting URL in the address
    bar. They typically have the form

    ```
    https://ucla.zoom.us/j/XXXXXXXXX
    ```

    where `XXXXXXXXX` is the meeting room number.

1.  If prompted download the Zoom client and/or enter the meeting room password.

You should now be recognized as a participant. In most cases you will bypass
the waiting room.

> If this is not the case, contact your instructor/TA, they will likely have to
> change their meeting settings.

[fig1]: image1.png
[fig2]: image2.png

---

## Method 2: using the Zoom client (Linux, Mac OS, Windows, ChromeOS, etc)

1.  If you have not done so already, download and install the Zoom client for
    your platform/device.

    > **Important:**
    >
    > If you have been attending Zoom meetings (UCLA Lectures, OH, etc.) via a
    > free account, you need to logout. Then follow the steps below.
    
1.  Open the Zoom client for your platform/device. Then look for the SSO sign in
    button.

    > If this option (SSO) is not present, you might need to click first the
    > _Sign in_ button (see figures below).

    | ![Zoom window1][fig3] |
    |:------------------------:|
    |_Fig 2: The Zoom linux client sign in window._ |

    | ![Zoom window2][fig4] |
    |:------------------------:|
    |_Fig 2: The Zoom linux client SSO sign in window._ |

1.  Click on the _Sign In with SSO_ button. In the next window, enter `ucla`
    next to the `.zoom.us` legend. You will be redirected to the UCLA login
    portal where you can enter your credentials.

    | ![Zoom window 3][fig5] |
    |:------------------------:|
    |_Fig 2: Redirection to the UCLA login portal._ |

[fig3]: image3.png
[fig4]: image4.png
[fig5]: image5.png

That should do it. Enjoy your lectures, OH, etc.

---


[Return to main course website][MAIN]

[syll]: ../syllabus/


[MAIN]: ..


