# Syllabus: Spring 2020 Math 61 Lecture 2

> If you are reading the online (html) version of this syllabus, [click
> here][pdf] to access a `.pdf` version of this document.[^if-you]

[pdf]: readme.pdf
[^if-you]: The embedded link does not work in the `.pdf` document.

---

## Course information

**Contact:** [rsalazar@math.ucla.edu][correo] (write "Math 61" in the subject).

**Dates & Time:** Monday, Wednesday, Friday, from 12:00 to 12:50 pm.

**Location:** Weather permitting we will alternate between _backyard, 
living room, and quite possibly a bedroom_.

**Office hours:** The following guidance has been provided by the math
department:

> "Instructors must offer two remote drop-in office hours per week at fixed
> times. **They should be scheduled in consultation with their students**
> (emphasis mine). In addition, instructors have to offer additional
> availability for students who cannot remotely attend the scheduled office
> hours."

In other words, I will set the office hours after I have _met_ with all of my
students during week 1 of instruction. Once the date/time has been set, an
official announcement will be sent to enrolled students, and it will be
published at [our CCLE class website (CCLE)][class-website].

**Teaching assistant(s):**

|**Section**| **T. A.**       |**Office**| **E-mail**                            |  
|:---------:|:----------------|:--------:|:--------------------------------------|  
| 2A, 2B    | Lyons, Clark    | `N/A`    | [`lyons@math.ucla.edu`][ta1]          |  
| 2C, 2D    | Gladkov, Nikita | `N/A`    | [`gladkovna@math.ucla.edu`][ta2]      |  

[correo]: mailto:rsalazar@math.ucla.edu
[class-website]: https://ccle.ucla.edu/course/view/20S-MATH61-2
[ta1]: mailto:lyons@math.ucla.edu
[ta2]: mailto:gladkovna@math.ucla.edu


## Course Description

From the math department [_General Course Outline_][GCO]:

> Math 61 has two goals. One goal is the introduction of certain basic
> mathematical concepts, such as equivalence relations, graphs, and trees. The
> other goal is to introduce non-mathematicians to abstraction and rigor in
> mathematics. Finite graphs are well-suited to this purpose. Exercises asking
> for simple proofs are assigned where appropriate.

[GCO]: https://www.math.ucla.edu/ugrad/courses/math/61


## Textbook & supplemental references

*   Johnsonbaugh, R., _Discrete Mathematics (8th Edition)_, Prentice-Hall.

*   [Levin, O., _Discrete Mathematics. An Open Introduction._,][sup1] 3rd
    edition. Open source textbook.

*   [Doerr, A. & Levasseur, Ken., _Applied Discrete Structures_.][sup2] 3rd
    edition - version 6. Open source textbook.

    > The latter two books will mostly be used as sources of examples as well as
    > exercises.

[sup1]: http://discrete.openmathbooks.org/dmoi3.html
[sup2]: http://faculty.uml.edu/klevasseur/ads2/


## Course outline

We will attempt to cover the material in the [mathematics department general
course outline][GCO]. Although an effort will be made to follow these
guidelines, material presented during lecture might be chosen from supplemental
references and/or material readily available online.


## CCLE and MyUCLA

This course will use a combination of a password-protected Internet site (CCLE),
as well as other regular (_i.e.,_ non-protected) sites to post course materials
and announcements. These materials can include the syllabus, handouts and
Internet links referenced in class.


## Reaching me via email

_During this current quarter I will be in charge of course(s) were enrollment is
higher than usual for a UCLA class._ In practice, this means that emails you
send to my email address might go unanswered for a rather long period of time.
Before sending me a message, you are encouraged to consult CCLE, as well as this
syllabus, as your question(s) might already be answered there.

If you feel that your question/issue has not been addressed in the documents
listed above, do feel free to send me a message, however keep the following in
mind:

*   I receive a high volume of messages throughout the day; in most cases it
    will be faster for you to get the information you seek if you reach out to
    me directly via Zoom (say during O.H., or right before/after lecture).
*   Messages with special keywords _skip_ my inbox. Use this to your advantage:
    if you add `Math 61` to your subject line, your message will find its way
    into a special folder that I check periodically. In most cases this reduces
    the time you have to wait before I reply to it.
*   **Messages with special attachments**, more specifically text files with
    specific file extensions (_e.g.,_ `.cpp`, `.h`, etc.), as well as messages
    containing some types of image attachments (_e.g.,_ `png`, `jpg`, `pdf`,
    etc.), **are sent directly to my _trash_ folder**.

    > This inconvenience brought to you by students that are under the
    > impression that deadlines do not apply to them (_e.g.,_ they attempt to
    > _late-submit_ their assignments via email attachments).
    >
    > _Special note:_ I understand that this quarter might be specially stressful
    > as we will not have a chance to interact personally. However, UCLA has
    > taken steps to make sure online platforms like CCLE, Gradescope, etc.,
    > are available to the UCLA population in general. To try to smooth out the
    > homework submission process, I will create _mock assignments_ with the
    > purpose that each and every single student in this class, determines the
    > best way to submit their work in a timely manner.


## Grading

_Grading method:_ quizzes (**Quiz**), homework assignments (**Hw**), midterms
(**Mid1**, **Mid2**), and final exam (**Final**).

*   _Quizzes:_ there will be **at least 8, but no more than 10** quizzes
    throughout the quarter. They will be based on one homework problem from the
    list assigned the prior week. This means that I reserve the right to make
    minor modifications on a problem that you were supposed to complete the week
    before, or to assign a completely different problem that can be solved using
    the same ideas/techniques you were expected to develop previously.

    These quizzes will be taken trough an online platform: CCLE or Gradescope.
    To accommodate for possible issues arising the use of these platforms **the
    two lowest quiz scores will not count towards the computation of your
    grade**.

    Each selected problem should be suitable to be completed in about 10
    minutes, but a time window of 15 minutes will be set to account for the time
    it takes to upload your work to these online platforms.

    _The overall weight assigned to the Quiz category is 40%._

*   _Homework assignments:_ homework will be assigned on a weekly basis
    throughout the quarter. You are encouraged to attempt to solve each and
    every single problem assigned during the week but **you will only have to
    submit two problems to Gradescope**.

    > Assuming _M_ students are enrolled in this course, and assuming _N_
    > problems were assigned during a week, at the end of the week I will
    > generate _M_ lists of 5 random numbers between 1 and _N_; then using the
    > email on record at CCLE, I will email every student one of these lists.
    > Students will then select 2 out of these 5 numbers and will _submit_ the
    > corresponding assignment problems to Gradescope.

    The submitted problems will be graded based on _accuracy_ and _form_. Note
    that you can choose to submit the easiest problems from the list, but you
    are still expected to present your argumentations to the best of your
    ability.

    For the first 3 weeks, you will be allowed to submit a picture of your
    work. After week 3 you will be required to _type_ your submissions directly
    on Gradescope. Guidance (_e,g.,_ handout, online resources) will be provided
    to you to familiarize you with the $\LaTeX$ document preparation system.

    **No late homework will be accepted** and **all of your homework scores will
    be used** in the computation of your overall score.

    _The overall weight assigned to the Homework category is 20%._

*   _Midterm:_ two fifty-minute midterms will be given on
    **April 27 (Monday week 5)**, and **May 25 (Monday week 9)**.

    For the time being they will be scheduled around 5 pm Pacific Time[^one].

    The submission process will be detailed later and it might include a
    two-step process:

    1. Submission of a _low resolution_ set of pictures to CCLE within 10
       minutes of the end of the exam; and
    2. Submission of either a set of high resolution image(s), or a clearly
       legible _.pdf_ file to Gradescope. This process will have to be completed
       within 24 hours of the end of the exam.

    _The overall weight assigned to every individual Midterm is 10%._

*   _Final exam:_ this cumulative exam will be _released_ on Tuesday, June 9 at
    8:00 am Pacific Time. Unlike the spring 2020 quarter, students **will not be
    given a 24-hr window to submit it.** Instead students will be allowed to
    start the exam within 24 hrs of the time described above, but once it is
    started, the submission process will have to be completed within 3 and half
    hours. The submission process might also have to be completed via two
    different platforms (_e.g.,_ CCLE and Gradescope).

    _The overall weight assigned to the Final category is 20%._

    > **Important:**  
    >  
    > _Failure to submit the final exam through the proper online platforms will
    > result in an automatic F!_

Your final score in the class will be determined by the following grading
breakdown:

|                                                                        |  
|:----------------------------------------------------------------------:|  
| 40% **Quiz** + 20% **Hw** + 10% **Mid1** + 10% **Mid2** + 20% **Final** |  

Overall scores determine letter grades according to the table below.

|                       |                      |                    |  
|:----------------------|:---------------------|:-------------------|  
| A+ (N/A)              | A (93.33% or higher) | A- (90% -- 93.32%) |  
| B+ (86.66% -- 89.99%) | B (83.33% -- 86.65%) | B- (80% -- 83.32%) |  
| C+ (76.66% -- 79.99%) | C (73.33% -- 76.65%) | C- (70% -- 73.32%) |  

The remaining grades will be assigned at my own discretion. Please do not
request exceptions.

> **Note:** students taking this class on a P/NP basis that attain an
> overall score in the C- range **are not** guaranteed a P letter grade.

**All grades are final when filed by the instructor on the Final Grade Report.**


### Policies and procedures (exams)

See individual grading categories (above) for the current state of these
policies. As I _iron out_ some of the details, this section will be updated.

**There will be no makeup midterms under any circumstances**, but I reserve the
right to apply alternate grading schemes on special circumstances. These
alternate grading schemes might include additional categories including but not
limited to: oral examinations and/or extra project assignments.

Exams will not be returned to you as you will not physically hand them over.
After receiving grading feedback, _any questions/concerns regarding how the
exam was graded must be submitted in writing_. You should be able to clearly
explain what mistake was made (if any), and why your argumentation is correct.


### Policies and procedures (assignments)

See individual grading categories (above) for the current state of these
policies. As I _iron out_ some of the details, this section will be updated.

Homework will be assigned on a weekly basis, and it will be collected the
following week. No late homework will be accepted for any reason.

Scores on homework assignments and quizzes will likely appear on Gradescope as
well as [MyUCLA][my-ucla]. Exams scores will only appear on MyUCLA. In case of
conflicting records (_e.g._ Gradescope reporting 8, whereas MyUCLA reporting 9),
MyUCLA is to be regarded as the _official_ record. It is your responsibility to
verify in a timely manner that the scores appearing therein are accurate.

[my-ucla]: https://my.ucla.edu/


## Accommodations

If you need any accommodations for a disability, please contact the UCLA
CAE[^two] [(`www.cae.ucla.edu`)][CAE]. Make sure to let me know as soon as
possible about necessary arrangements that need to be made.

[CAE]: https://www.cae.ucla.edu
[^two]: Center for Accessible Education

|                                                      |
|:----------------------------------------------------:|
| Course Syllabus Subject to Update by the Instructor. |


[^one]: I might be able to offer a 50 minute exam to be completed within a 24hr
  time frame akin to most winter 2020 finals. But I'm still looking into specif
  details about a possible implementation.
