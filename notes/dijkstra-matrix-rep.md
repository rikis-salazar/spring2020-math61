---
title: 'Shortest Path & Graph Representations'
author: 'Ricardo Salazar'
date: 'May 29, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

# Dijkstra's shortest-path algorithm

## The algorithm

Finds length of shortest path from $a$ to $z$ in a connected, weighted graph.

~~~~~ {.cpp}
dijkstra(w, a, z, L) {       // Assumes weights are positive.
    L(a) = 0
    for all vertices x != a
        L(x) = INFINITY

    T = set of all vertices  // T is set of vertices whose shortest
                             // distance from a has not been found.
    while ( z in T ) {
        choose v in T with minimum L(v)  // <-- This is line 9
        remove v from T      // In set notation T = T − {v}
        for each x in T adjacent to v update labels
            L(x) = min{ L(x) , L(v) + w(v,x) }
    }
}
~~~~~

## Let's practice

Verify that the initialization in _Dijkstra's_ algorithm produces the graph in
figure \ref{fig:1} below, then find the shortest path between $a$ and $z$.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./dijkstra-init.png}
  \caption{Initialization in shortest path algorithm.}\label{fig:1}
\end{figure}


```






```

## Let's practice

Find the shortest path between $a$ and $z$ while keeping track of the vertices traveled.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./dijkstra-init-2.png}
  \caption{Initialization in shortest path algorithm.}\label{fig:2}
\end{figure}


```






```

## Why it works

::: {.alert title="Theorem"} :::
_Dijkstra's_ shortest path algorithm correctly finds the length of a shortest
path from $a$ to $z$.
::::::::::::

_Sketch of the proof:_

```











```

# Representation of graphs

## Adjacency Matrix

How to obtain the \textsc{Adjacency Matrix} of a graph:

*   Select ordering of vertices and use it to label rows and columns.
*   The entry in row $i$, column $j$ is
    -    the number of edges incident on $i$ and $j$ (if $i\neq j$);
    -    twice the number of loops incident on $i$ (if $i=j$).


::: {.alert title="Example"} :::
Find the adjacency matrix of the following graph.
::::::::::::

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.25\textheight]{./adj-matrix.png}
  \caption{A connected graph.}\label{fig:4}
\end{figure}


## Let's practice

::: {.alert title="Exercise"} :::
Find the adjacency matrix $\mathbb{A}$ of the graph below, then compute
$\mathbb{A}^{2}$ and $\mathbb{A}^{3}$.
::::::::::::

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.25\textheight]{./adj-matrix-2.png}
  \caption{A simple connected graph.}\label{fig:3}
\end{figure}

```






```

## Final remarks

::: {.alert title="Theorem"} :::
If $\mathbb{A}$ is the adjacency matrix of a simple graph, the $ij$th entry of
$\mathbb{A}^{n}$ is equal to the number of paths of length $n$ from vertex $i$
to vertex $j$, $n= 1,2,\dots$.
::::::::::::

::: {.alert title="Another representation"} :::
To obtain the \textsc{Incidence Matrix} of a graph:

*   label the rows with vertices (in arbitrary order),
*   label the columns with edges (in arbitrary order).

The entry for row $v$ and column $e$ is 1 if $e$ is incident on $v$ and 0
otherwise.
::::::::::::

What is the incidence matrix of the graph in figure \ref{fig:4}?

```





```
