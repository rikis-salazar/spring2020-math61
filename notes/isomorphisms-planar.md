---
title: 'Isomorphisms & Planarity'
author: 'Ricardo Salazar'
date: 'Jun 1, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

# Isomorphisms of Graphs

## A graph in _plain English_

Follow the instructions below to draw the graph $G$.

```

```

> Draw and label five vertices $a$, $b$, $c$, $d$, and $e$. Connect $a$ and $b$,
> $b$ and $c$, $c$ and $d$, $d$ and $e$, and $a$ and $e$.

```












```

## The concept of isomorphic graphs

::: {.alert title="Definition"} :::
Graphs $G_{1}$ and $G_{2}$ are \textsc{isomorphic} if there is

*   a bijection $f$ from the vertices of $G_{1}$ to the vertices of $G_{2}$, and
*   a bijection $g$ from the edges of $G_{1}$ to the edges of $G_{2}$,

so that

> an edge $e$ is incident on $v$ and $w$ in $G_{1}$ 

if and only if

> the edge $g(e)$ is incident on $f(v)$ and $f(w)$ in $G_{2}$.

```

```

The pair $f$ and $g$ is called an \textsc{isomorphism} of $G_{1}$ onto $G_{2}$.
::::::::::::


## Let's practice

::: {.alert title="Example"} :::
Any pair of graphs resulting from the _"A graph in plain English"_ slide.
::::::::::::

```












```

## Some highlights

::: {.alert title="Theorem"} :::
Graphs $G_{1}$ and $G_{2}$ are isomorphic if and only if for some ordering of
their vertices, their adjacency matrices are equal.
::::::::::::

_Sketch of proof:_

```











```

## Some remarks

::: {.alert title="Invariants"} :::
A property $P$ is an invariant if whenever $G_{1}$ and $G_{2}$ are isomorphic graphs:

```

```

> If $G_{1}$ has property $P$, $G_{2}$ also has property $P$.
::::::::::::

By the definition of isomorphism, the following are invariants:

*   "has $n$ vertices", and
*   "has $m$ edges".

Other invariants?

*   "has ..."
*   "has ..."


## Let's practice

Show that the following graphs are not isomorphic.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.25\textheight]{./invariant1.png}
  \caption{Nonisomorphic graphs.}\label{fig:1}
\end{figure}

```








```


## Let's practice

Show that the following graphs are not isomorphic.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./invariant2.png}
  \caption{Nonisomorphic graphs.}\label{fig:2}
\end{figure}

```








```


## Let's practice

Show that the following graphs are not isomorphic.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./invariant3.png}
  \caption{Nonisomorphic graphs.}\label{fig:3}
\end{figure}

```








```


# Planar Graphs

## No edge crossings

::: {.alert title="Definition"} :::
A graph is \textsc{planar} if it can be drawn in the plane without its edges
crossing.
::::::::::::

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.25\textheight]{./planar1.png}
  \caption{A planar graph with 4 faces ($A$, $B$, $C$, $D$), 8 edges, and 6
      vertices.}\label{fig:4}
\end{figure}


## _Euler's_ formula for graphs

::: {.alert title="Theorem"} :::
If $G$ is connected and planar with $e$ edges, $v$ vertices, and $f$ faces, then
$$f = e − v + 2.$$
::::::::::::

::: {.alert title="Application"} :::
Use _Euler's_ formula to show that $K_{3,3}$ is not planar\footnote{\tiny So,
are you finally ready to have this conversation?}.

```









```
::::::::::::


## _Euler's_ formula for graphs (cont)

### Sketch of the proof

```
















```


