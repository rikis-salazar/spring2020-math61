---
title: 'Graphs'
author: 'Ricardo Salazar'
date: 'May 11, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

# Examples & basic concepts

## The traveling ~~salesperson~~ Wizard

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[width=0.67\textwidth]{./middle-earth.png}
  \caption{Gandalf's inspection problem: travel all roads to determine
    \emph{Sauron's} strength (e.g., number of \emph{orcs}, \emph{wargs},
    \emph{balrogs}, etc.).}
\end{figure}


## The traveling ~~salesperson~~ Wizard (cont)

### The problem

Here are the conditions:

*   _Gandalf_ starts at _Hobbiton_, the center of _The Shire_.[^one]

*   Wants to travel each road exactly once.

*   Wants to visit all cities: _Rivendell_, _Angmar_, _Hobbiton_, _Bree_,
    _Edoras_, _Isengard_, _Dol Amroth_, _Minas Tirith_, and _Minas Morgul_.

*   Wants to end the trip back at _Hobbiton_.[^two]

```


```

Is this possible?[^twopointfive]

```




```

[^one]: He and _Bilbo_ went back after the Battle Of the Five Armies (BOFA).
[^two]: To commemorate _Bilbo's_ 111-th birthday.
[^twopointfive]: _Fun fact:_ Bilbo was in his early 50's right after BOFA.


## Definitions

*   A \textsc{Graph} $G$ consists of a set $V$ of vertices and a set $E$ of
    edges such that each edge $e \in E$ is associated with an **unordered pair**
    of vertices.  

    If only one $e \in E$ is associated with $v,w\in V$, we write $e = (v,w)$ or
    $e = (w, v)$.

*   A \textsc{Directed Graph} (_digraph_) $G$ consists of a set $V$ of vertices 
    and a set $E$ of edges such that each edge $e \in E$ is associated with an
    **ordered pair** of vertices.  

    If only one $e \in E$ is associated with $v,w\in V$, we write $e = (v,w)$.

    ```

    ```

\begin{quote}
  {\tiny \textbf{Note:} In this context $(v,w)$ denotes an edge not an ordered
  pair.}
\end{quote}


## Definitions (cont)

*   If an edge $e$ is associated with $v$ and $w$, we say that

    -   $e$ is \textsc{Incident On} v and w;

    -   $v$ and $w$ are \textsc{Incident On} $e$; and

    -   $v$ and $w$ are \textsc{Adjacent Vertices}.

    ```


    ```

*   If G is a graph with vertices V and edges E, we write $G = (V, E)$.

    ```


    ```

**Note:** Unless specified otherwise $|E|,|V|<\infty$ and $V\neq\emptyset$.


## Let's practice

### Examples

1.  Find the set of vertices and edges associated to figure 1 (Gandalf's
    traveling problem).

    ```






    ```

2.  Represent graphically $G = (V,E)$ from the problem above.

    ```








    ```

## How many graphs?

Consider the graph

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.25\textheight]{./graph1.png}
  \caption{Parallel edges, loops, and isolated vertices.}\label{fig:two}
\end{figure}

Here we have
\begin{align*}
  V & = \{v_{1},v_{2},\dots,v_{6}\}, & E & = \{e_{1}, e_{2}, \dots, e_{5}\},
\end{align*}
and clearly $G = (V,E)$ is one graph (not three of them).

```




```


## More definitions

*   Distinct edges associated with the same pair of vertices are called
    \textsc{Parallel Edges}.

*   An edge incident on a single vertex is called a \textsc{Loop}.

*   A vertex that is not incident on any edge is called and \textsc{Isolated
    Vertex}.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.25\textheight]{./graph1.png}
  \caption{
    $e_{1}$ and $e_{2}$ are parallel edges; $e_{3}$ is a loop;
    $v_{4}$ is an isolated vertex.
      }
\end{figure}

*   A graph with neither loops nor parallel edges is called a \textsc{Simple
    Graph}.


## A new type of graph?

_Problem:_ Make holes in sheets of metal using computer controlled drills.
\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.20\textheight]{./holes.png}
  \caption{
      A sheet of metal with holes for bolts.
      }
\end{figure}
We can use a graph to model it.
\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./graph2.png}
  \caption{
      A graph with \emph{weights} representing the time it takes to move a
      drill.
      }
\end{figure}


## Some remarks & definitions

*   Vertices of the graph in figure 3 correspond to the holes in figure 2.

*   Every pair of vertices is connected by an edge (_i.e.,_ it is _complete_).

*   The time to move the drill press between adjacent holes is written on each
    edge.

    -   A graph with numbers on the edges is called a \textsc{Weighted Graph}.

    -   If edge $e$ is labeled $k$, we say that the weight of $e$ is $k$.

        _E.g.:_ In figure 3 the weight of edge $(c,e)$ is 5.

    -   In a weighted graph, the \textsc{Length of a Path} is the sum of the
        weights of the edges in the path.

        _E.g.:_ In Figure 3 the length of the path that starts at $a$, visits
        $c$, and terminates at $b$ is 8.

## Some remarks & definitions (cont)

*   In this problem, the length of the path $v_{1},v_{2},\dots,v_{n}$,
    represents the time it takes the drill press to start at hole $h_{1}$ and
    then move to $h_{2},\dots,h_{n}$, where hole $h_{i}$ corresponds to vertex
    $v_{i}$.

*   A path of minimum length that visits every vertex exactly one time
    represents the \textsc{Optimal Path} for the drill press to follow.

*   The following table helps us find the optimal path assuming the first hole
    is at vertex $a$, and the last hole at vertex $e$.

    | _Path_    | _Length_ |   | _Path_    | _Length_ |
    |:----------|:--------:|:-:|:----------|:--------:|
    | a,b,c,d,e |       21 |   | a,c,d,b,e |       26 |
    | a,b,d,c,e |       28 |   | a,d,b,c,e |       27 |
    | a,c,b,d,e |       24 |   | a,d,c,b,e |       22 |

*   This is a version of **the traveling salesman problem**.


# Some Special Graphs

## Complete graphs

::: {.alert title="Definition"} :::
The \textsc{Complete Graph on} $n$ \textsc{Vertices}, denoted $K_{n}$, is the
simple graph with $n$ vertices in which there is an edge between every pair of
distinct vertices.
::::::::::::

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \phantom{x}
  \hfill
  \includegraphics[height=0.30\textheight]{./k-4.png}
  \hfill
  \includegraphics[height=0.30\textheight]{./graph2.png}
  \hfill
  \phantom{x}
  \caption{
      \emph{Left:} a graphic representation of the complete graph $K_{4}$;
      \emph{Right:} a weighted version of the complete graph $K_{5}$.
      }
\end{figure}


## Bipartite graphs

::: {.alert title="Definition"} :::
A graph $G=(V,E)$ is \textsc{Bipartite} if there exist subsets $V_{1}$ and
$V_{2}$ (either possibly empty) of $V$ such that $V_{1} \cap V_{2} = \emptyset$,
$V_{1} \cup V_{2} = V$, and each edge in $E$ is incident on one vertex in
$V_{1}$ and one vertex in $V_{2}$.
::::::::::::

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \phantom{x}
  \hfill
  \includegraphics[height=0.30\textheight]{./bipartite.png}
  \hfill
  \includegraphics[height=0.30\textheight]{./k-2-4.png}
  \hfill
  \phantom{x}
  \caption{
      \emph{Left:} a bipartite graph with 5 vertices;
      \emph{Right:} the complete bipartite graph $K_{2,4}$.
      }
\end{figure}


## Let's practice

::: {.alert title="Exercise"} :::
Prove that the graph below is not bipartite.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./non-bipartite.png}
  \caption{
      A non bipartite graph.
  }
\end{figure}
::::::::::::

```






```

## Complete Bipartite graphs

::: {.alert title="Definition"} :::
The \textsc{Complete Bipartite Graph on $m$ and $n$ Vertices}, denoted
$K_{m,n}$, is the simple graph whose vertex set is partitioned into sets $V_{1}$
with $m$ vertices and $V_{2}$ with $n$ vertices in which the edge set consists
of all edges of the form $(v_{1}, v_{2})$ with $v_{1} \in V_{1}$ and $v_{2} \in
V_{2}$.
::::::::::::

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./k-3-3.png}
  \caption{
      The complete \st{non-planar}\footnote{It is indeed \emph{non-planar}, but
      you are not ready to have this conversation.} bipartite graph $K_{3,3}$.
  }
\end{figure}

## Final remarks

::: {.alert title="Exercise"} :::
For what values of $n$ is $K_{n}$ bipartite?
::::::::::::

```













```


# Paths & Cycles

## Paths

::: {.alert title="Definition"} :::
Let $v_{0}$ and $v_{n}$ be vertices in a graph. A \textsc{Path} from $v_{0}$ to
$v_{n}$ of Length $n$ is an alternating sequence of $n + 1$ vertices and $n$
edges beginning with vertex $v_{0}$ and ending with vertex $v_{n}$, 
\begin{equation*}
  (v_{0}, e_{1}, v_{1}, e_{2}, v_{2}, \dots, v_{n−1}, e_{n}, v_{n}), 
\end{equation*}
in which edge $e_{i}$ is incident on vertices $v_{i−1}$ and $v_{i}$ for $i =
1,..., n$.
::::::::::::

_Note:_ In plain English this says 

*   start at vertex $v_{0}$;
*   go along edge $e_{1}$ to $v_{1}$;
*   go along edge $e_{2}$ to $v_{2}$;
*   and so on.


## Let's practice

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./graph3.png}
  \caption{
      A \emph{connected}\footnote{It is \emph{connected}, \st{but you are not
      ready} and you are ready to have this conversation.} graph.
  }\label{fig:one}
\end{figure}

*   Is $(1, e_{1}, 2, e_{2}, 3, e_{3}, 4, e_{4}, 2)$ a path? If so, what is its
    length?

    ```

    ```

*   Is $(6)$ a path? If so, what is its length?

    ```

    ```

*   Is there a path connecting any two vertices of the graph?


## Connected graphs

::: {.alert title="Definition"} :::
A graph $G$ is \textsc{Connected} if given any vertices $v$ and $w$ in $G$,
there is a path from $v$ to $w$.
::::::::

```

```

::: {.alert title="Example"} :::
The graph in figure \ref{fig:one} is connected.
::::::::

```

```

::: {.alert title="Example"} :::
The graph in figure \ref{fig:two} is not connected.
::::::::


## Let's practice

Let $G$ be the graph whose vertex set consists of the states in the map below.
Put an edge between states $v$ and $w$ if $v$ and $w$ share a border.

Is $G$ connected?

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.50\textheight]{./usa.pdf}
  \caption{
    \texttt{2b || !2b == ?}
  }
\end{figure}

## Let's practice

Let $G$ be the graph whose vertex set consists of the states in the map below.
Put an edge between states $v$ and $w$ if $v$ and $w$ share a border.

Is $G$ connected?

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.55\textheight]{./mex.png}
  \caption{
    \texttt{2b || !2b == ?}
  }
\end{figure}


## Subgraphs

::: {.alert title="Definition"} :::
Let $G = (V, E)$ be a graph. We call $(V', E')$ a \text{Subgraph} of $G$ if

1.  $V' \subseteq V$ and $E' \subseteq  E$.
1.  For every edge $e' \in E'$, if $e'$ is incident on $v'$ and $w'$, then $v',
    w' \in V'$.
::::::::

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \phantom{x}
  \hfill
  \includegraphics[height=0.30\textheight]{./graph4.png}
  \hfill
  \includegraphics[height=0.30\textheight]{./graph5.png}
  \hfill
  \phantom{x}
  \caption{
      A subgraph $G'$ of the graph $G$.
      }
\end{figure}


## Let's practice

::: {.alert title="Exercise"} :::
Find all subgraphs of $K_{2}$ having at least one vertex.
::::::::

```






```

::: {.alert title="Definition"} :::
Let $G$ be a graph and let $v$ be a vertex in $G$. The subgraph $G'$ of $G$
consisting of all edges and vertices in $G$ that are contained in some path
beginning at $v$ is called the component of $G$ containing $v$.
::::::::


## Let's practice

### Exercises

*   How many components does the graph in figure \ref{fig:one} have?

```




```

*   Let $G$ be the graph of Figure \ref{fig:two}. Find

    - The component of $G$ containing $v_{3}$.
    - The component of $G$ containing $v_{4}$.
    - The component of $G$ containing $v_{5}$.

```








```


## More definitions

::: {.alert title="Definition"} :::
Let v and w be vertices in a graph G.

*   A \textsc{Simple Path} from $v$ to $w$ is a path from $v$ to $w$ with no
    repeated vertices.

    ```



    ```

*   A \textsc{Cycle} (or circuit) is a path of nonzero length from $v$ to $v$
    with no repeated edges.

    ```



    ```

*   A \textsc{Simple Cycle} is a cycle from $v$ to $v$ in which, except for the
    beginning and ending vertices that are both equal to $v$, there are no
    repeated vertices.

    ```




    ```
::::::::


## Let's practice

### Exercise

Let $G$ be the graph in figure \ref{fig:one}. Fill in the information in the
table below.

```







```

| Path | Simple Path? | Cycle? | Simple Cycle? |  
|:-------------|:-----:|:-----:|:-----:|  
| `(6,5,2,4,3,2,1)` | | | |  
| `(6,5,2,4)`       | | | |  
| `(2,6,5,2,4,3,2)` | | | |  
| `(5,6,2,5)`       | | | |  
| `(7)`             | | | |  


## The Königsberg Bridge Problem

Two islands lying in the _Pregel River_ were connected to each other and the
river banks by bridges (see figure \ref{fig:three}). The problem is to start at
any location---A, B, C, or D; walk over each bridge exactly once; then return to
the starting location.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \phantom{x}
  \hfill
  \includegraphics[height=0.30\textheight]{./konigsberg.png}
  \hfill
  \includegraphics[height=0.30\textheight]{./konigsberg-graph.png}
  \hfill
  \phantom{x}
  \caption{
      The bridges of \emph{Königsberg} and a corresponding graph model.
      }\label{fig:three}
\end{figure}


## Let's ~~practice~~ write down some observations


## Euler Cycles

::: {.alert title="Definition"} :::
A cycle in a graph $G$ that includes all of the edges and all of the vertices of $G$
is called an \textsc{Euler Cycle}.
::::::::

```


```

::: {.alert title="Definition"} :::
The \textsc{Degree of a Vertex} $v$, $\delta(v)$, is the number of edges
incident on $v$.

By definition, each loop on $v$ contributes 2 to the degree of $v$.
::::::::

```


```

### Theorem: Euler cycles (necessary condition)

If a graph G has an Euler cycle, then G is connected and every vertex has even
degree.


## The _"sufficient"_ portion of the proof is _constructive_.

Let $G$ be the graph in the figure below. Does $G$ have an Euler cycle? If so, find an
Euler cycle for $G$.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./find-euler.png}
  \caption{
      Can you find an Euler cycle?
  }
\end{figure}

```







```

## Euler Cycles (cont)

### Theorem: Euler cycles (sufficient condition)

If $G$ is a connected graph and every vertex has even degree, then $G$ has an
Euler cycle.  

```














```

## Other facts

::: {.alert title="Theorem"} :::
If $G$ is a graph with $m$ edges and vertices $\{v_{1},v_{2},\dots,v_{n}\}$, then
\begin{equation*}
  \sum_{i=1}^{n} \delta(v_{i}) = 2m.
\end{equation*}
In particular, the sum of the degrees of all the vertices in a graph is even.
::::::::

```









```


## Other facts (cont)

::: {.alert title="Corolary"} :::
In any graph, the number of vertices of odd degree is even.
::::::::

```




```

::: {.alert title="Theorem"} :::
If a graph contains a cycle from $v$ to $v$, then it contains a simple cycle
from $v$ to $v$.
::::::::

```




```


## Other facts (cont)

::: {.alert title="Theorem"} :::
A graph $G$ has a path with no repeated edges from $v$ to $w$ ($v \neq w$)
containing all the edges and vertices if and only

*   $G$ is connected; and
*   $v$ and $w$ are the only vertices having odd degree.
::::::::

```











```
