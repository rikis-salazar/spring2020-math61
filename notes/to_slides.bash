#!/bin/bash
#
pandoc -f markdown+fenced_code_attributes+fancy_lists+startnum+pipe_tables+yaml_metadata_block+tex_math_dollars+link_attributes --highlight-style tango --pdf-engine xelatex -t beamer -V theme:CambridgeUS --variable monofont=Inconsolata --filter pandoc-beamer-block $1.md -o $1_slides.pdf
