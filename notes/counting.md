---
title: 'Counting Principles'
author: 'Ricardo Salazar'
date: 'April 22, 2020'
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

# The Multiplication Principle

## The statement

### Multiplication Principle

If an activity can be constructed in $t$ **successive** steps, where

*   step 1 can be done in $n_{1}$ ways;

*   step 2 can then be done in $n_{2}$ ways;

*   . . . ; and

*   step $t$ can then be done in $n_{t}$ ways;

then the number of different possible activities is $n_{1}\cdot n_{2} \cdots
n_{t}$.


## Cardinality of Cartesian Product

::: {.alert title="Example"} :::
Given $X$ and $Y$, finite sets (_i.e.,_ $|X|<\infty$, $|Y|<\infty$). What is the
cardinality of $X\times Y$?
::::::::::::

To solve this using multiplication principle we need:

*   Activity: form an ordered pair $(x,y)$ sequentially.

*   Step 1: choose the first entry;

*   Step 2: then choose the second entry.


## Cardinality of Cartesian Product

Let

$$X = \{x_{1},x_{2},\dots,x_{n}\},\qquad Y = \{y_{1},y_{2},\dots,y_{m}\}.$$

Need to find

*   In how many ways can step 1 be done?

*   In how many ways can step 2 be done?

The total number of ordered pairs is
$$| X \times Y | =$$


## Using _Decision_ Trees

Let $X = \{x_{1},x_{2},\dots,x_{n}\}$, $Y = \{y_{1},y_{2},\dots,y_{m}\}$. What
is $| X \times Y |$?

### Solution

$$\star$$

$$x_{1}\qquad\qquad x_{2}\qquad\qquad \cdots \qquad\qquad x_{n}$$

$$y_{1}\: y_{2}\: \cdots \: y_{m} \qquad  
  y_{1}\: y_{2}\: \cdots \: y_{m} \qquad \cdots \qquad
  y_{1}\: y_{2}\: \cdots \: y_{m}$$

```



```


## Multiple applications

::: {.alert title="Melissa Virus"} :::
In late 1990s a computer virus named Melissa spread by email. The virus sent 50
copies [of itself] to other email addresses; each of these addresses then sent
50 copies; each of these ...
::::::::::::

Activity: Count copies of Melissa.

*   Step 0: Original Melissa.

*   Step 1: 1st generation copies.

*   Step 2: 2nd generation copies.

```



```


What is total number of Melissas after 4 generations?


## Counting strings

::: {.alert title="Example"} :::
a)  How many strings of length 4 can be formed using ABCDE?

    ```




    ```

a)  How many strings of part (a) begin with the letter B?

    ```




    ```

a)  How many strings of part (a) do not begin with the letter B?

    ```




    ```

::::::::::::


## Counting strings (revisited)

::: {.alert title="Example"} :::
a)  How many strings of length 4 **if repetitions are not allowed**?

    ```




    ```

a)  How many strings of part (a) begin with the letter E?

    ```




    ```

a)  How many strings of part (a) do not begin with the letter E?

    ```




    ```

::::::::::::


# The Addition Principle

## The statement

### Addition Principle

Suppose that $X_{1},\dots,X_{t}$ are sets and that the $i$-th set $X_{i}$ has
$n_{i}$ elements.

```


```

If $\{X_{1},\dots,X_{t}\}$ is a pairwise disjoint family (_i.e.,_ if $i\neq j$,
$X_{i}\cap X_{j} = \emptyset$), the number of possible elements that can be
selected from $X_{1}$ or $X_{2}$ or $\cdots$ or $X_{t}$ is
\begin{equation}\nonumber
  n_{1} + n_{2} + \cdots + n_{t}.
\end{equation}

```


```

Equivalently,
\begin{equation}\nonumber
  |X_{1}\cup X_{2}\cup\cdots\cup X_{t}| = n_{1} + n_{2} + \cdots + n_{t}.
\end{equation}

```


```


## Committees

::: {.alert title="Example"} :::

A six-person committee composed of _Abigail_, _Beatriz_, _Citlali_, _Daneli_,
_Elisa_, and _Frida_ is to select a chairperson, secretary, and treasurer.

a)  In how many ways can this be done?

    ```






    ```

a)  In how many ways if either _Citlali_ or _Frida_ must be chairperson?

    ```






    ```

::::::::::::


## Committees (cont)

::: {.alert title="Example (cont)"} :::

A six-person committee ...  _Abigail_, ... and _Frida_ ...

a)  In how many ways if _Elisa_ must hold one of the offices?

    ```






    ```

a)  In how many ways if both _Abigail_ and _Beatriz_ must hold office?

    ```






    ```

::::::::::::


# The Inclusion Exclusion Principle

## The statement

### Inclusion-Exclusion principle for 2 sets

If $X$ and $Y$ finite sets, then

\begin{equation}\nonumber
  | X \cup Y | = | X | + | Y | - | X \cap Y |.
\end{equation}


### Proof

Recall that if $A$ and $B$ are disjoint, then $|A\cup B| = |A| + |B|$.

```









```


## Committees (revisited)

::: {.alert title="Example"} :::

A six-person committee composed of _Abigail_, _Beatriz_, _Citlali_, _Daneli_,
_Elisa_, and _Frida_ is to select a chairperson, secretary, and treasurer. How
many selections are there in which either _Citlali_ or _Daneli_ or both are
officers?

::::::::::::

### Solution

Let $X = \{\text{Commitees in which Citlali is officer}\}$.  
Let $Y = \{\text{Commitees in which Daneli is officer}\}$.

Note that $X\cap Y \neq \emptyset$.

```








```
