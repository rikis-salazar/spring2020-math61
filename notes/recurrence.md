---
title: 'Recurrence Relations'
author: 'Ricardo Salazar'
date: 'May 5, 2020'
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

# Examples & basic concepts

## A basic sequence & definitions

::: {.alert title="The instructions"} :::
Generate a sequence:

*   Start with 5.
*   Given any term, add 3 to the next term.

Let us list the terms:

```





```

::::::::::::

::: {.alert title="Recurrence Relation"} :::
A _recurrence relation_ for the sequence $a_{0}, a_{1}, \dots$ is an equation
that relates $a_{n}$ to certain of its predecessors $a_{0}, a_{1}, \dots,
a_{n−1}$.  
_Initial conditions_ for the sequence $a_{0}, a_{1}, \dots$ are explicitly given
values for a finite number of the terms of the sequence.
::::::::::::


## Let's practice

### Examples

1.  The _Fibonacci sequence_ is defined by the recurrence relation $f_{n} =
    f_{n−1} + f_{n−2}$, $n \ge 3$, and initial conditions $f_{1} = 1$, $f_{2} =
    1$.

    ```





    ```

2.  A person invests $1000 at 12 percent interest compounded annually. If
    $A_{n}$ represents the amount at the end of $n$ years, find a recurrence
    relation and initial conditions that define the sequence $\{A_{n}\}$.

    ```







    ```

## Let's practice

### Examples

3.  Let $S_{n}$ denote the number of $n$-bit strings that do not contain the
    pattern 111. Develop a recurrence relation for $S_{1}, S_{2}, \dots$ and
    initial conditions that define the sequence $S$.

    ```











 

    ```

## Tower of _Hanoi_

### The setup

Three pegs are mounted on a board and $n$ disks of various increasing diameters
with holes in their centers are placed on one peg.

| ![The Hanoi puzzle][hanoi] |
|:--------------------------:|
| Figure 1: _Tower of Hanoi with $n$ disks._ |

If a disk is on a peg, only a smaller disk can be placed on top of it.

[hanoi]: ./hanoi.png


## Tower of _Hanoi_ (cont)

### The puzzle

Given all the disks stacked on one peg (see figure 1), the problem is to
transfer the disks to another peg by moving one disk at a time.

```














```


# Solving Recurrence Relations

## Iterating recurrence relations

### Say hi to our old friend

Solve the recurrence relation
$$a_{n} = a_{n-1} + 3, \qquad n\ge 2,$$
subject to the initial condition $a_{1} = 5$.

```











```

## Cardinality of power sets (revisited)

### Say hi to our old friend

Find a recurrence relation for the cardinality of the power set of a non-empty
set with $n$ elements. Then solve this recurrence relation.

```














```


## Tower of Hanoi (revisited)

### Say hi to our old friend

Find an explicit formula for $c_{n}$, the minimum number of moves in which the
$n$-disk Tower of Hanoi puzzle can be solved.

```














```


## Linear Homogeneous with Constant Coefficients

::: {.alert title="Definition"} :::
A _linear homogeneous recurrence relation of order $k$ with constant
coefficients_ is a recurrence relation of the form
$$a_{n} = c_{1}a_{n−1} + c_{2}a_{n−2} + \cdots + c_{k}a_{n−k}, \qquad c_{k} \neq
0,\qquad n\ge k.$$
::::::::::::

::: {.alert title="Note"} :::
A linear homogeneous recurrence relation of order $k$ with constant coefficients
together with the $k$ initial conditions
$$a_{0} = C_{0},\quad a_{1} = C_{1},\quad \dots, \quad a_{k−1} = C_{k−1},$$
defines a sequence $a_{0}, a_{1}, \dots, a_{n}, \dots$.
::::::::::::


## Let's practice

### Examples

Determine whether or not the following recurrence relations are linear
and homogenous. If so, determine the order.

```

```

*   The relation $a_{n} = 3a_{n−1} a_{n−2}$

    ```



    ```

*   The relation $S_{n} = 2S_{n−1}$

    ```



    ```

*   The relation $f_{n} = f_{n−1} + f_{n-2}$

    ```



    ```

*   The relation $a_{n} = a_{n−1} + 2n$

    ```



    ```

## Cardinality of power sets (re-revisited)

### Say hi to our old friend

~~Find a recurrence relation for the cardinality of the power set of a non-empty
set with $n$ elements. Then solve this recurrence relation.~~

We know the answer already... but let's try this just _for fun_.

```













```

## The _Fibonacci_ sequence (revisited)

### Say hi to our old friend

Find an explicit formula for $f_{n}$, the $n$-th term in the _Fibonacci_
sequence.

```















```

## Theorem

Let
\begin{equation}\label{eq:one}
  a_{n} = c_{1}a_{n-1} + c_{2}a_{n-2}.
\end{equation}

*   If the sequences $\mathcal{S}$ and $\mathcal{T}$ are solutions of
    \eqref{eq:one}, then $\mathcal{U} = \alpha \mathcal{S} + \beta \mathcal{T}$
    is also a solution of \eqref{eq:one}.

*   If $r$ is a root of
    \begin{equation}\label{eq:two}
      t^{2} − c_{1}t − c_{2} = 0,
    \end{equation}
    then $a_{n} = r^{n}$, $n = 0, 1, \dots$, is a solution of
    \eqref{eq:one}.

*   If $\mathcal{A}$ is the sequence defined by \eqref{eq:one},
    \begin{equation*}
      a_{0} = C_{0},\qquad a_{1} = C_{1},
    \end{equation*}
    and $r_{1}$ and $r_{2}$ are roots of
    \eqref{eq:two} with $r_{1}\neq r_{2}$, then there exist constants $\alpha$
    and $\beta$ such that
    \begin{equation*}
      a_{n} = \alpha r_{1}^{n} + \beta r_{2}^{n},\qquad n = 0, 1, \dots.
    \end{equation*}
