---
title: 'Hamilton Cycles'
author: 'Ricardo Salazar'
date: 'May 18, 2020'
header-includes:
  - \usepackage{caption}
  - \usepackage{soul}
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

# Hamilton Cycles

## A 3D puzzle

_William Hamilton_ marketed a puzzle in the mid-1800s.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./dodecahedron.png}
  \caption{Hamilton's puzzle.}\label{fig:1}
\end{figure}

Each corner bore the name of a city. The problem was to 

*   start at any city;
*   travel along the edges;
*   visit each city exactly one time; and
*   return to the initial city.


## Let's ~~practice~~ solve the puzzle

The graph of the edges of the _dodecahedron_ is given below (figure \ref{fig:2})
in _planar_ form.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./dodecahedron-planar.png}
  \caption{The graph of Hamilton's puzzle.}\label{fig:2}
\end{figure}

We can solve the puzzle if we can find a cycle that contains each vertex
exactly once (except for the starting and ending vertex that appears twice).


## Hamiltonian Cycles

::: {.alert title="Definition"} :::
A \textsc{Hamiltonian Cycle} is a cycle that contains each vertex of a graph
exactly once, except for the starting and ending vertex that appears twice.
::::::::::::

```


```

::: {.alert title="Example"} :::
\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./dodecahedron-hamilton.png}
  \caption{A \emph{Hamiltonian Cycle} for the \emph{dodecahedron}.}\label{fig:3}
\end{figure}
::::::::::::


## Let's practice

### Examples

Find a Hamiltonian cycle for the graph in figure \ref{fig:4}.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.30\textheight]{./hamiltonian1.png}
  \caption{A graph with a \emph{Hamiltonian cycle}.}\label{fig:4}
\end{figure}

```







```

## Let's practice

### Examples

Show that the graph in figure \ref{fig:5} does not contain a Hamiltonian cycle.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.25\textheight]{./no-hamiltonian.png}
  \caption{A graph with a \emph{Hamiltonian cycle}.}\label{fig:5}
\end{figure}

```








```


## Let's practice

### Examples

*   Use an _eliminate-edges-and-count_ argument to claim that the graph in
    figure \ref{fig:6} does not contain a Hamiltonian cycle.\footnote{\tiny Are
    you sure you are not overcounting?}

    \begin{figure}[!h]
      \captionsetup{justification=centering}
      \includegraphics[height=0.25\textheight]{./fake-no-hamiltonian.png}
      \caption{A graph with \st{no} a \emph{Hamiltonian cycle}.}\label{fig:6}
    \end{figure}

```






```


## Let's practice

### Examples

Show that the graph in figure \ref{fig:7} does not contain a Hamiltonian cycle.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.25\textheight]{./no-hamiltonian2.png}
  \caption{A graph with \st{no} a \emph{Hamiltonian cycle}.}\label{fig:7}
\end{figure}

```








```


## The traveling ~~wizard~~ salesperson

::: {.alert title="The Problem"} :::
Given a weighted graph $G$, find a minimum-length Hamiltonian cycle in $G$.
::::::::

```


```

::: {.alert title="In Plain English"} :::
If vertices equal cities and edge weights equal distances, the problem is _to
find a shortest route in which the salesperson can visit each city one time,
starting and ending at the same city_.
::::::::


## The Knight's tour

In chess, a knight’s move consists of moving two squares horizontally or
vertically and then moving one square in the perpendicular direction.

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.25\textheight]{./knight-moves.png}
  \caption{The knight's legal moves in chess.}\label{fig:n}
\end{figure}

A _knight’s tour_ of an $n \times n$ board begins at some square, visits each
square exactly once making legal moves, and returns to the initial square.

**The problem:** to determine for which $n$ a knight’s tour exists.


## The Knight's tour (cont)

Use a graph to model the problem:

*   Let the squares of the board be the vertices of the graph.
*   Place an edge between two vertices if the corresponding squares on the board
    represent a legal move for the knight.

For an $n \times n$ board, the resulting graph is denoted $GK_{n}$.

::: {.alert title="Exercise"} :::
Use the board below to _construct_ $GK_{4}$.
::::::::

\begin{figure}[!h]
  \captionsetup{justification=centering}
  \includegraphics[height=0.25\textheight]{./4x4-board.png}
  \caption{The knight's legal moves in chess.}\label{fig:n}
\end{figure}

## The Knight's tour (cont)


::: {.alert title="Proposition (Necessary Condition)"} :::
If $GK_{n}$ has a Hamiltonian cycle, $n$ is even.
::::::::

```





```

::: {.alert title="Counterexample (Not Sufficient)"} :::
$GK_{2}$ and $GK_{4}$ do not have Hamiltonian cycles.
::::::::

```





```
