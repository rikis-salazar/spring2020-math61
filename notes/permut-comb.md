---
title: 'Permutations & Combinations'
author: 'Ricardo Salazar'
date: 'April 29, 2020'
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

# Permutations

## Example: elections in _The Shire_.

::: {.alert title="Example"} :::
Four candidates, _Frodo_, _Sam_, _Meriadoc_, and _Peregrin_, are running for the
same office. How many distinct ballots will there be if the names will appear in
random positions?
::::::::::::

Solution:
```










```

## Permutations

::: {.alert title="Definition"} :::
A _permutation_ of $n$ distinct elements $x_{1},\dots,x_{n}$ is an ordering of
the $n$ elements $x_{1},\dots,x_{n}$.
::::::::::::

::: {.alert title="Theorem"} :::
Given a set with $n$ elements, there are $n!$ permutations of the members of the
set.
::::::::::::

_Proof:_

```







```


## Let's practice

### Examples

1.  How many permutations of the letters _ABCDEF_ contain the letters _DEF_
    together in that order?

    ```






    ```

1.  How many permutations of the letters _ABCDEF_ contain the letters _DEF_
    together in any order?

    ```







    ```

## Let's practice

### Examples

3.  In how many ways can six persons wait in a line?

    ```






    ```

3.  In how many ways[^one] can six persons be seated around a circular table?

    ```







    ```

[^one]: If a seating is obtained from another seating by having everyone move
  $n$ seats clockwise, the seatings are considered identical.


## $r$-Permutations

::: {.alert title="Definition"} :::
An $r$-_permutation_ of $n$ distinct elements $x_{1},\dots,x_{n}$ is an ordering
of an $r$-element subset of $\{x_{1},\dots,x_{n}\}$. The number of
$r$-permutations of a set of $n$ distinct elements is denoted $P(n,r)$, or
$_{n}P_{r}$, or $P^{n}_{r}$, or $^{n}P_{r}$, or $P_{n,r}$.
::::::::::::

::: {.alert title="Theorem"} :::
The number of $r$-permutations of a set of $n$ distinct objects is
$$P(n,r) = n(n-1)\cdots(n-r+1) = \frac{n!}{(n-r)!}.$$
::::::::::::

_Proof:_

```




```

## Let's practice

### Examples

1.  In how many ways can we select a _chairperson_, _vice-chairperson_,
    _secretary_, and _treasurer_ from a group of 10 persons?

    ```






    ```

1.  In how many ways can seven distinct _Dwarves_ and five distinct _Elves_ wait
    in line if no two _Elves_ stand together?

    ```







    ```


# Combinations

## Selections Disregarding Order

::: {.alert title="Example"} :::
In how many ways can five students choose a three person committee if there are
no special titles?
::::::::::::

_Solution:_

```





```

::: {.alert title="Definition"} :::
Let $X = \{x_{1},\dots,x_{n}\}$ be a set containing $n$ distinct elements.

*   An $r$-combination of $X$ is an unordered selection of $r$ elements of $X$.
*   The number of $r$-combinations of a set of $n$ distinct elements is denoted
    $C(n,r)$, or $\binom{n}{r}$, or $_{n}C_{r}$, or $C^{n}_{r}$, or $^{n}C_{r}$,
    or $C_{n,r}$.
::::::::::::


## $r$-Combinations

::: {.alert title="Theorem"} :::
The number of $r$-combinations of a set of $n$ distinct objects is
$$C(n,r) =\frac{P(n,r)}{r!} = \frac{n!}{r!(n-r)!},\qquad r\le n.$$
::::::::::::

_Proof:_

```









```

## Let's practice

::: {.alert title="Example"} :::
1.  How many eight-bit strings contain exactly four 1’s?

    ```






    ```

1.  In how many ways can we select a committee of two women and three men from a
    group of five distinct women and six distinct men?

    ```








    ```

::::::::::::


# Generalizations

## Scrambling words

::: {.alert title="Example"} :::
How many strings can be formed using the letters in the word  
M I S S I I S S I P P I?
::::::::::::

_Solution via combinations:_

```











```


## Scrambling words

::: {.alert title="Example"} :::
How many strings can be formed using the letters in the word  
M I S S I I S S I P P I?
::::::::::::

_Solution via fake permutations:_

```











```


## Permutations with _indistinguishable_ objects

::: {.alert title="Theorem"} :::
Suppose that a sequence $S$ of $n$ items has $n_{1}$ identical objects of type
1, $n_{2}$ identical objects of type 2, ..., and $n_{t}$ identical objects of
type $t$. Then the number of orderings of $S$ is
$$\frac{n!}{n_{1}!n_{2}!\cdots n_{t}!}$$
::::::::::::

::: {.alert title="Example"} :::
How many strings can be formed using the letters in the word  
P A R A N G A R I C U T I R I M I C U A R O?
::::::::::::

```





```


## Let's practice

::: {.alert title="Example"} :::
Consider three books: _computer science_, physics, and history. Suppose the
library has at least six copies of each. In how many ways can we select six
books?

```













```

::::::::::::


## Let's practice

::: {.alert title="Example"} :::
By counting subsets of a set with $n$ elements, prove that
$$ \binom{n}{0} + \binom{n}{1} + \binom{n}{2} + \cdots + \binom{n}{n-2} +
\binom{n}{n-1} + \binom{n}{n} = 2^{n}.$$
::::::::::::

_Solution:_

```









-
```

