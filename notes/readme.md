# Lecture notes

This is the place to find _pdf_ files of the documents I use during lecture.
Whenever possible, two versions will be provided: the _clean_ version containing
the initial state of the document; as well as the _annotated_ version with all
the extra ~~doodles~~ remarks made during lecture.


## Week 10

*   Friday: [_Graphs: Planarity & Euler's formula for graphs_][w10-l3-a].

*   Wednesday: [_Graphs: isomorphisms_][w10-l2-a].

*   Monday: [_Graphs: matrix representations_][w10-l1-a].

## Week 9

*   Friday: [_Graphs: a shortest path algorithm_][w9-l3-a].

*   Wednesday: [_Graphs: Hamiltonian cycles (part2)_][w9-l2-a].

*   Monday: No lecture.

## Week 8

*   Friday: [_Graphs: Hamiltonian cycles (part 1)_][w8-l3-a].

*   Wednesday: [_Graphs: degree of vertices and Euler Cycles_][w8-l2-a].

*   Monday: [_Graphs: paths and cycles_][w8-l1-a].

## Week 7

*   Friday: [_Graphs: more definitions, special graphs_][w7-l3-a].

*   Wednesday: [_Graphs: basic concepts_][w7-l2-a].

*   Monday: [_Recurrence relations (part 3)_][w7-l1-a].

---

## Week 6

*   Friday: [_Recurrence relations (part 2)_][w6-l3-a].

*   Wednesday: [_Pigeonhole principle & Recurrence relations_][w6-l2-a].

*   Monday: [_More counting. Pigeonhole principle_][w6-l1-a].

---

## Week 5

*   Friday: [_Combinations and other examples_][w5-l3-a].

*   Wednesday: [_Permutations & combinations_][w5-l2-a].

*   Monday: Midterm 1 (no class notes).

---

## Week 4

*   Friday: [_Counting: addition principle, inclusion-exclusion_][w4-l3-a].

*   Wednesday: [_Counting: multiplication and addition principles_][w4-l2-a].

*   Monday: [_Matrices of relations annotated_][w4-l1-a].


## Week 3

*   Friday: [_Equivalence relations annotated_][w3-l3-a].

*   Wednesday: [_Relations annotated (part 2)_][w3-l2-a].

*   Monday: [_Relations annotated (part 1)_][w3-l1-a].

## Week 2

*   Wednesday & Friday: [_Functions annotated (part 2)_][w2-l2-a], [_Sequences
    and strings annotated_][w2-l23].

*   Monday: [_Sets annotated (part 2)_][w2-l1-a1], [_Functions_][w2-l1-c],
    [_Functions annotated (part 1)_][w2-l1-a2].

## Week 1

*   Monday: _no lecture notes; see lecture recordings & [course syllabus][syl]._

*   Wednesday: [_Induction_][w1-l2-c], [_Induction annotated_][w1-l2-a].

    > _Errata:_ In the notes you will find the _words_ trimino/triminoes. The
    > correct words in English are _tromino/trominoes_.

*   Friday: [_Sets_][w1-l3-c], [_Sets annotated (part 1)_][w1-l3-a].

[syl]: ../syllabus/
[w1-l2-c]: 01-wed.pdf
[w1-l2-a]: 01-wed-ann.pdf
[w1-l3-c]: 01-fri.pdf
[w1-l3-a]: 01-fri-ann.pdf

[w2-l1-c]: 02-mon.pdf
[w2-l1-a1]: 02-mon-ann1.pdf
[w2-l1-a2]: 02-mon-ann2.pdf
[w2-l2-a]: 02-wed-ann.pdf
[w2-l23]: 02-fri-ann.pdf

[w3-l1-a]: 03-mon-ann.pdf
[w3-l2-a]: 03-wed-ann.pdf
[w3-l3-a]: 03-fri-ann.pdf

[w4-l1-a]: 04-mon-ann.pdf
[w4-l2-a]: 04-wed-ann.pdf
[w4-l3-a]: 04-fri-ann.pdf

[w5-l2-a]: 05-wed-ann.pdf
[w5-l3-a]: 05-fri-ann.pdf

[w6-l1-a]: 06-mon-ann.pdf
[w6-l2-a]: 06-wed-ann.pdf
[w6-l3-a]: 06-fri-ann.pdf

[w7-l1-a]: 07-mon-ann.pdf
[w7-l2-a]: 07-wed-ann.pdf
[w7-l3-a]: 07-fri-ann.pdf

[w8-l1-a]: 08-mon-ann.pdf
[w8-l2-a]: 08-wed-ann.pdf
[w8-l3-a]: 08-fri-ann.pdf

[w9-l2-a]: 09-wed-ann.pdf
[w9-l3-a]: 09-fri-ann.pdf

[w10-l1-a]: 10-mon-ann.pdf
[w10-l2-a]: 10-wed-ann.pdf
[w10-l3-a]: 10-fri-ann.pdf

---

[Return to main course website][MAIN]


[MAIN]: ..

