---
title: 'The Pigeonhole Principle'
author: 'Ricardo Salazar'
date: 'May 1, 2020'
pandoc-beamer-block:
  - classes: [info]
  - classes: [alert]
    type: alert
---

# Pigeonhole Principle

## First form

::: {.alert title="Goal"} :::
To answer the question: _Is there an item having a given property?_
::::::::::::

::: {.alert title="Pigeonhole Principle"} :::
If $n$ pigeons fly into $k$ pigeonholes and $k < n$, some pigeonhole contains at
least two pigeons.
::::::::::::

_Proof:_

```







```

\begin{quote}
  {\tiny Note: It does not explain how to find the \emph{overcrowded}
  pigeonhole.}
\end{quote}


## Let's practice

### Same names

1.  Ten persons have first names _Alice_, _Angie_, and _Alma_ and last names
    _Galvez_, _Hess_, and _Alvarez_. Show that at least two persons have the
    same first and last names.

    $1^{st}$ _Soln:_\hfill $2^{nd}$ _Soln:_\hfill \phantom{x}

    ```












    ```

## Second form

::: {.alert title="Pigeonhole Principle"} :::
If $f$ is a function from a finite set $X$ to a finite set $Y$ and $|X| > |Y|$,
then $f$ cannot be _one-to-one_. In other words, there exists $x_{1},x_{2}\in
X$, with $f(x_{1}) = f(x_{2})$, yet $x_{1} \neq x_{2}$.
::::::::::::

_Proof:_

```









```


## Let's practice

### Examples

2.  Show that if we select 151 distinct computer science courses numbered
    between 1 and 300 inclusive, at least two are consecutively numbered.

    ```











 

    ```

## Let's practice

### Examples

3.  An inventory consists of a list of 89 items. 50 of them are marked
    _“available”_. The rest are marked _“unavailable"_. Show there are at least
    two available items nine items apart.

    ```













    ```


## Third form

::: {.alert title="Pigeonhole Principle"} :::
Let $f$ be a function from a finite set $X$ into a finite set $Y$. Suppose that
$|X| = n$ and $|Y| = m$. Let $k = \lceil n/m \rceil$. Then there are at least
$k$ distinct values $a_{1},a_{2},\dots,a_{k}\in X$ such that
$$f(a_{1}) = f(a_{2}) = \cdots = f(a_{n}).$$
::::::::::::

_Proof by contradiction:_ Let $Y = \{y_{1},..., y_{m}\}$. Then

* there are at most $(k−1)$ distinct points $x\in X$ with $f(x) = y_{1}$;
* there are at most $(k−1)$ distinct points $x\in X$ with $f(x) = y_{2}$;
* $\vdots$
* there are at most $(k−1)$ distinct points $x\in X$ with $f(x) = y_{m}$.

Thus there are at most $m(k−1)$ members in $X$. But
$$m(k−1) < m \frac{n}{m} = n.$$
```






```


