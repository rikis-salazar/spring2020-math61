# Math 61 \hfill Intro to Discrete Structures \hfill Spring 2020

\begin{center}
  {\Large
    \textsc{Pop Quiz 2}\\[1 ex]
    %\today
    April 20, 2020
  }
\end{center}

\vspace{10 mm}


> ```
>
>
> Name: _________________________________________________________________
>
>
>
> Signature: ____________________________________________________________
>
>
> ```

\vspace{10 mm}

1.  Upload a high quality image/pdf of your answer to question 1 of the
    corresponding CCLE quiz.

